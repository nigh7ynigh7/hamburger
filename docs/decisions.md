
# Minhas decisões

Antes de qualquer outra coisa, vou descrever o stack que estou usando para 
este projeto
e a motivação por trás da escolha.
O back estará em **negrito**, e o front estará em _itálico_.

- _Chakra-UI_
    - Qualquer framework de GUI seria legal.
    - Eu, pessoalmente, tenho mais experiência com _Ant.Design_, mas escolhi este framework,
    o _Chakra-UI_ porque achei ele bem interessante e relativamente fácil de usar.
    - ... E fácil de configurar.
- _React_
    - Pelo fato que é o framework que tenho mais conforto.
- _Typescript_
    - Facilitar o desenvolvimento com sugestões... e _tipificar_ o meu código.
- **Spring-Web/Boot/Data**
    - Ter a possibilidade de ter injeção de dependência
    - Facilmente expor os dados através de um _rest api_.
- **H2 Database**
    - Ter um banco simples só para armazenar dados em memória.

## Backend

### Modelagem de dados

Tenho 6 tabelas modeladas conforme o seguinte diagrama:

```
      ┌─────────────────┐
      │SimpleClientOrder├─────┐
      └──────┬──────────┘     │
             │                │
             │                │
  ┌──────────┴───────┐        │
  │IngredientPerOrder│        │
  └──────────┬───────┘        │
             │                │
     ┌───────┴───┐       ┌────┴────┐
  ┌──┤Ingredients│       │Hamburger│
  │  └───────────┘       └────┬────┘
  │                           │
  │   ┌────────────────────┐  │
  └───┤HamburgerIngredients├──┘
      └────────────────────┘
```

Sendo que

- um `SimpleClientOrder` para vários `IngredientPerOrder`,
- um `Hamburger` para vários `SimpleClientOrder`,
- um `Ingredients` para vários `IngredientPerOrder`,
- um `Ingredients` para vários `HamburgerIngredients`
- e um `Hamburger` para vários `HamburgerIngredients`

Com essas coleções, as seguintes possibilidades são permitidas:

- Armazenar um pedido de apenas 1 hamburger
- Não afetar o preço do ingrediente ao aplicar os descontos
- Ter um hamburger com vários ingredientes
- Ter um ingrediente aplicado a vários hamburgers

Toda tabela tem um `id`, ou seja,
não estou usando chaves compostas para facilitar o desenvolvimento,
dado o tempo que tenho.

### Estrutura do projeto

Este projeto tem uma estrutura genérica que permite _cruds_ para toda
implementando a interface `Identifiable<IdType>`.
Isso facilita a criação de novos crud caso eles apareçam.

**Boundary** é termo que designei para _endpoints_ neste projeto.

Segue a estrutura em geral do projeto:

```
  ┌────────────┐
  │Identifiable│
  └─────┬──────┘
        │
      ┌─┴──────────────┐
      │identifiables...│
      └─┬──────────────┘
        │
      ┌─┴──────────────┐
      │RootRepositories│
      └─┬──────────────┘
        │
      ┌─┴────────────────────┐
      │CommonAbstractServices│
      └─┬──┬─────────────────┘
        │  │
        │  │ ┌─────────┐        ┌────┐
        │  └─┤ services├────────┤impl│
        │    └─────────┘        └────┘
        │
      ┌─┴────────────┐
      │CommonBoundary│
      └──────────────┘
```

### Descontos

Descontos, porém, funcionam de uma forma diferente.

A estrutura foi feita pensando em degraus, sendo que um pedido só pode ter vários descontos consecutivos. Segue o mapeamento:

```
   ┌──────────────────┐
   │DiscountProcessors│
   └───┬──────────────┘
       │
       │
       │
       │      ┌───────┐
       └──────┤Impl...│ cada impl é um "step"
              └───────┘
```

O `DiscountProcessors` tem um membro chamado `weight` que determina a ordem das operações. A que tem maior peso é executa primeira.

### A cola de tudo

A cola deste projeto todo é o `OrderService`, que pega o `dto` enviado ao API, e
agrega as informações para processar os descontos e... por final, gerar o modelo que
é persistido no banco e mostrado para o usuário.

### Alguns pontos a serem considerados

- Sei que `FetchType.EAGER` não é eficiente, mas optei para usar o recurso nesse projeto levando em consideração o escopo dele e do tempo que tive para montar tudo.
- `@JsonProperty(access = WRITE)` foi usado para evitar referências circulares.
- UUID foi usado para gerar um código que pode ser aplicado no URL da aplicação.
- Uma estrutura genérica também foi criada para expor alguns _enums_.
- Não ha autenticação

## Front

O _front_ tem dois lados:

- O lado de fazer pedidos
- E o lado para gerenciar os hambúrgueres  e os ingredientes.

A tela raiz do projeto expande aos poucos até mostrar o botão para enviar o pedido.
Depois que o pedido for enviado, aparece outra tela com um botão para visualizar o pedido feito.

Ao acessar `/admin`, o usuário terá de digitar um usuário e senha.
Já que esse projeto não tem autenticação - é permitido digitar qualquer coisa que a interface aceitará.

Ao entrar na tela de admin: há um menu lateral com os seguintes itens:

- _home_ - para visualizar os pedidos
- _ingredients_ - para gerenciar os ingredientes
- _hamburgers_ - para gerenciar os hambúrgueres
- _leave_ - para voltar para a tela principal (de fazer pedidos)

A parte de fazer foi feito/desenvolvido foi pensando em sabilidade e praticidade:

- O pedido feito em forma de acordião, deixando o usuário customizar seu hamburger.
- Uma tela com os detalhes do pedido para mostrar o que foi adiquirido.

A parte do admin foi desenvolvido pensando em acesso aos dados de forma fácil e intuítivo.

### Parte técnica

