# README (ptbr)

## Configuração do Projeto

O projeto foi configurado com `spring-boot`, `jpa/hibernate` usando o banco em memória H2, ``

Endpoints que o sistema libera para acesso (fora o rest):

- `/swagger-ui.html`
- `/h2-console`
    - url do banco: `jdbc:h2:mem:testdb`
    - usuario: `hello`
    - senha: `world`*

## Como contruir o projeto?

basta executar:

    $ mvn clean package

ou com o mvn já providenciado pelo repo ou com o seu próprio maven.

## Como executar o projeto?

executar:

    $ java -jar target/hamjoint-0.0.1-snaphsot.jar

# Readme (en)

## Project configuration

The project at hand is configured with `spring-boot`, `jpa/hibernate` using the in-memory database H2, ``

Other endpoints this project currently exposes:

- `/swagger-ui.html`
- `/h2-console`
    - db url: `jdbc:h2:mem:testdb`
    - user: `hello`
    - password: `world`

## How to build the project

All you have to do is execute:

    $ mvn clean pacakge

Or use the maven already provided by this repository.

## How to execute this project?

    $ java -jar target/hamjoint-0.0.1-snaphsot.jar