package obrauli.io.hamjoint.data.services.impl;

import obrauli.io.hamjoint.data.dto.CompactUserRequest;
import obrauli.io.hamjoint.data.dto.DiscountStep;
import obrauli.io.hamjoint.data.dto.IngredientCount;
import obrauli.io.hamjoint.data.dto.IngredientPrice;
import obrauli.io.hamjoint.data.models.Hamburger;
import obrauli.io.hamjoint.data.models.HamburgerIngredient;
import obrauli.io.hamjoint.data.models.Ingredient;
import obrauli.io.hamjoint.data.services.crud.HamburgerService;
import obrauli.io.hamjoint.data.services.crud.IngredientService;
import obrauli.io.hamjoint.data.services.crud.SimpleOrderService;
import obrauli.io.hamjoint.discount.DiscountProcessors;
import obrauli.io.hamjoint.discount.beans.LightPromo;
import obrauli.io.hamjoint.discount.beans.SoMuchBacon;
import obrauli.io.hamjoint.discount.beans.SoMuchCheese;
import obrauli.io.hamjoint.enums.IngredientType;
import obrauli.io.hamjoint.exceptions.SelfAssertionException;
import obrauli.io.hamjoint.utils.BigDecimalUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(value = MockitoExtension.class)
public class OrderServiceImplTest {

    @Mock
    HamburgerService hamburgerService;

    @Mock
    IngredientService ingredientService;

    @Mock
    SimpleOrderService simpleOrderService;

    OrderServiceImpl orderService;

    private static CompactUserRequest compactUserRequest(){
        CompactUserRequest compactUserRequest = new CompactUserRequest();
        compactUserRequest.setForWhom("me");
        compactUserRequest.setHamburgerId(1L);
        compactUserRequest.setOverwriteIngredients(new ArrayList<>());
        compactUserRequest.getOverwriteIngredients().add(new IngredientCount());
        compactUserRequest.getOverwriteIngredients().get(0).setQuantity(BigDecimal.TEN);
        compactUserRequest.getOverwriteIngredients().get(0).setIngredientId(1L);
        return compactUserRequest;
    }

    private static Hamburger hamburger() {
        Hamburger output = new Hamburger();
        output.setId(1L);
        output.setName("hamburger");
        output.setActive(true);
        output.setCreatedAt(new Date());
        output.setUpdatedAt(new Date());
        output.setHamburgerIngredients(new HashSet<>());

        Ingredient i = ingredient();

        HamburgerIngredient hi = new HamburgerIngredient();
        hi.setHamburger(output);
        hi.setIngredient(i);
        hi.setQuantity(BigDecimal.ONE);
        hi.setId(1L);

        output.setHamburgerIngredients(Set.of(hi));
        return output;
    }

    private static Ingredient ingredient() {
        Ingredient i = new Ingredient();
        i.setId(1L);
        i.setValue(new BigDecimal("1.5"));
        i.setAllowFractional(false);
        return i;
    }

    private DiscountStep discountStep() {
        DiscountStep ds = new DiscountStep();
        IngredientPrice ingredientPrice = new IngredientPrice();
        Ingredient i = ingredient();
        ingredientPrice.setQuantity(BigDecimal.ONE);
        ingredientPrice.setIngredient(ingredient());
        ingredientPrice.setStartPrice(i.getValue());
        ingredientPrice.setEndPrice(i.getValue());
        ds.getIngredients().add(ingredientPrice);
        ds.setStartingValue(ds.getCalculatedTotal());
        ds.setEndingValue(ds.getStartingValue());
        return ds;
    }

    private List<DiscountProcessors> getDiscountProcessors() {
        return List.of(
                new SoMuchBacon(),
                new SoMuchCheese(),
                new LightPromo()
        );
    }

    @BeforeEach
    public void init() {
        this.orderService = Mockito.spy(
                new OrderServiceImpl(
                        this.hamburgerService,
                        this.ingredientService,
                        this.simpleOrderService,
                        getDiscountProcessors()
                )
        );
    }

    @Test
    public void validateUserRequestShouldFailOnNull () {
        Assertions.assertThrows(NullPointerException.class, () -> {
            this.orderService.validateUserRequest(null);
        });
    }

    @Test
    public void validateUserRequestShouldFailOnEmptyUserReq () {
        CompactUserRequest compactUserRequest = new CompactUserRequest();
        Assertions.assertThrows(SelfAssertionException.class, () -> {
            this.orderService.validateUserRequest(compactUserRequest);
        });
    }

    @Test
    public void validateUserRequestShouldFailOnNoIdentifier () {
        CompactUserRequest compactUserRequest = new CompactUserRequest();
        compactUserRequest.setHamburgerId(1L);
        Assertions.assertThrows(SelfAssertionException.class, () -> {
            this.orderService.validateUserRequest(compactUserRequest);
        });
    }

    @Test
    public void validateUserRequestShouldFailOnNoHamburger () {
        CompactUserRequest compactUserRequest = new CompactUserRequest();
        compactUserRequest.setForWhom("me");
        Assertions.assertThrows(SelfAssertionException.class, () -> {
            this.orderService.validateUserRequest(compactUserRequest);
        });
    }

    @Test
    public void validateUserRequestShouldPassWithHamburger () {
        CompactUserRequest compactUserRequest = new CompactUserRequest();
        compactUserRequest.setForWhom("me");
        compactUserRequest.setHamburgerId(1L);
        Assertions.assertDoesNotThrow(() -> {
            this.orderService.validateUserRequest(compactUserRequest);
        });
    }

    @Test
    public void validateUserRequestShouldFailWithIncompleteIngredientOverwritesNoQuantity () {
        CompactUserRequest compactUserRequest = new CompactUserRequest();
        compactUserRequest.setForWhom("me");
        compactUserRequest.setHamburgerId(1L);
        compactUserRequest.setOverwriteIngredients(new ArrayList<>());
        compactUserRequest.getOverwriteIngredients().add(new IngredientCount());
        compactUserRequest.getOverwriteIngredients().get(0).setIngredientId(1L);
        Assertions.assertThrows(SelfAssertionException.class, () -> {
            this.orderService.validateUserRequest(compactUserRequest);
        });
    }

    @Test
    public void validateUserRequestShouldFailWithIncompleteIngredientOverwritesNoIngredient () {
        CompactUserRequest compactUserRequest = new CompactUserRequest();
        compactUserRequest.setForWhom("me");
        compactUserRequest.setHamburgerId(1L);
        compactUserRequest.setOverwriteIngredients(new ArrayList<>());
        compactUserRequest.getOverwriteIngredients().add(new IngredientCount());
        compactUserRequest.getOverwriteIngredients().get(0).setQuantity(BigDecimal.TEN);
        Assertions.assertThrows(SelfAssertionException.class, () -> {
            this.orderService.validateUserRequest(compactUserRequest);
        });
    }

    @Test
    public void validateUserRequestShouldPassWithIngredientOverwrites () {
        CompactUserRequest compactUserRequest = new CompactUserRequest();
        compactUserRequest.setForWhom("me");
        compactUserRequest.setHamburgerId(1L);
        compactUserRequest.setOverwriteIngredients(new ArrayList<>());
        compactUserRequest.getOverwriteIngredients().add(new IngredientCount());
        compactUserRequest.getOverwriteIngredients().get(0).setQuantity(BigDecimal.TEN);
        compactUserRequest.getOverwriteIngredients().get(0).setIngredientId(1L);
        Assertions.assertDoesNotThrow(() -> {
            this.orderService.validateUserRequest(compactUserRequest);
        });
    }

    @Test
    public void makeOrderShouldPassWhenAllMethodsExceptValidationMocked() {
        doReturn(hamburger()).when(this.hamburgerService).findById(1L);
        doReturn(null).when(this.orderService).buildFirstStep(any(), any());
        doNothing().when(this.orderService).applyInPlaceFilterOnStep(any());
        doNothing().when(this.orderService).secondaryStepValidationAndInitialization(any());
        doReturn(Collections.emptyList()).when(this.orderService).applyDiscountsAndGenerateSteps(any());
        doReturn(null).when(this.orderService).persist(any(), any(), any());

        var output = this.orderService.makeOrder(compactUserRequest());

        verify(this.hamburgerService, times(1)).findById(compactUserRequest().getHamburgerId());
    }

    @Test
    public void buildFirstStepShouldReturnListWhenOverwriting() {
        Hamburger h = hamburger();
        Ingredient i = h.getHamburgerIngredients().iterator().next().getIngredient();
        doReturn(i).when(this.ingredientService).findById(i.getId());
        DiscountStep step = this.orderService.buildFirstStep(compactUserRequest(), h);
        Assertions.assertNotNull(step.getIngredients());
        Assertions.assertEquals(1, step.getIngredients().size());
        verify(this.ingredientService, times(1)).findById(i.getId());
    }


    @Test
    public void buildFirstStepShouldReturnListWhenNotOverwriting() {
        Hamburger h = hamburger();
        Ingredient i = h.getHamburgerIngredients().iterator().next().getIngredient();
        CompactUserRequest cur = compactUserRequest();
        cur.getOverwriteIngredients().get(0).setIngredientId(2L);
        Ingredient i2 = new Ingredient();
        i2.setId(2L);

        doReturn(i2).when(this.ingredientService).findById(i2.getId());

        DiscountStep step = this.orderService.buildFirstStep(cur, h);
        Assertions.assertNotNull(step.getIngredients());
        Assertions.assertEquals(2, step.getIngredients().size());
    }

    @Test
    public void applyInPlaceFilterOnStepShouldNotModifyValue() {
        DiscountStep step = discountStep();
        this.orderService.applyInPlaceFilterOnStep(step);
        Assertions.assertEquals(1, step.getIngredients().size());
    }

    @Test
    public void applyInPlaceFilterOnStepShouldRemoveOnZeroQuantity() {
        DiscountStep step = discountStep();
        step.getIngredients().iterator().next().setQuantity(BigDecimal.ZERO);
        this.orderService.applyInPlaceFilterOnStep(step);
        Assertions.assertEquals(0, step.getIngredients().size());
    }

    @Test
    public void applyInPlaceFilterOnStepShouldFloorWhenFractional() {
        DiscountStep step = discountStep();
        step.getIngredients().iterator().next().getIngredient().setAllowFractional(false);
        this.orderService.applyInPlaceFilterOnStep(step);
        Assertions.assertEquals(new BigDecimal("1"), step.getIngredients().iterator().next().getQuantity());
    }

    @Test
    public void secondaryStepValidationAndInitializationShouldFailWhenNoIngredientsAreFound(){
        DiscountStep step = discountStep();
        step.setIngredients(Collections.emptySet());
        Assertions.assertThrows(SelfAssertionException.class, () -> {
            this.orderService.secondaryStepValidationAndInitialization(step);
        });
    }

    @Test
    public void secondaryStepValidationAndInitializationShouldFailWhenNoIngredientsAreNull(){
        DiscountStep step = discountStep();
        step.setIngredients(null);
        Assertions.assertThrows(SelfAssertionException.class, () -> {
            this.orderService.secondaryStepValidationAndInitialization(step);
        });
    }

    @Test
    public void secondaryStepValidationAndInitializationShouldModifyValues(){
        DiscountStep step = discountStep();
        this.orderService.secondaryStepValidationAndInitialization(step);
        Assertions.assertEquals(new BigDecimal("1.5"), step.getStartingValue());
        Assertions.assertEquals(new BigDecimal("1.5"), step.getEndingValue());
    }

    @Test
    public void secondaryStepValidationAndInitializationShouldModifyValuesAndDoubleWhenQuantityIs2(){
        DiscountStep step = discountStep();
        step.getIngredients().iterator().next().setQuantity(BigDecimalUtils.TWO);
        this.orderService.secondaryStepValidationAndInitialization(step);
        Assertions.assertEquals(new BigDecimal("3.0"), step.getStartingValue());
        Assertions.assertEquals(new BigDecimal("3.0"), step.getEndingValue());
    }

    @Test
    public void applyDiscountsAndGenerateStepsAppliesNoDiscountFromEmptyList() {
        this.orderService.applyDiscountsAndGenerateSteps(discountStep());
    }

    @Test
    void applyDiscountsAndGenerateStepsAppliesNoCheeseDiscount() throws Exception {
        DiscountStep step = discountStep();
        IngredientPrice i = step.getIngredients().iterator().next();
        BigDecimal endPrice = i.getEndPrice();
        i.setQuantity(new BigDecimal("3"));
        List<DiscountStep> steps = this.orderService.applyDiscountsAndGenerateSteps(step);
        Assertions.assertEquals(1, steps.size());
        Assertions.assertEquals(endPrice, steps.get(0).getIngredients().iterator().next().getEndPrice());
    }

    @Test
    void applyDiscountsAndGenerateStepsAppliesACheeseDiscount() throws Exception {
        DiscountStep step = discountStep();
        IngredientPrice i = step.getIngredients().iterator().next();
        i.getIngredient().setIngredientType(IngredientType.DAIRY_CHEESE);
        BigDecimal endPrice = i.getEndPrice();
        i.setQuantity(new BigDecimal("3"));
        List<DiscountStep> steps = this.orderService.applyDiscountsAndGenerateSteps(step);
        Assertions.assertEquals(2, steps.size());
        Assertions.assertNotEquals(endPrice, steps.get(1).getIngredients().iterator().next().getEndPrice());
    }

    @Test
    void persistShouldHappenWithTheGivenValues() {
        Hamburger ham = hamburger();
        Ingredient i = ham.getHamburgerIngredients().iterator().next().getIngredient();
        doReturn(ham).when(this.hamburgerService).findById(ham.getId());
        doReturn(i).when(this.ingredientService).findById(i.getId());
        when(this.simpleOrderService.save(any()))
                .thenAnswer((invocation) -> invocation.getArguments()[0]);

        String output = this.orderService.makeOrder(compactUserRequest());

        Assertions.assertNotNull(output);
        Assertions.assertFalse(ObjectUtils.isEmpty(output));
    }
}
