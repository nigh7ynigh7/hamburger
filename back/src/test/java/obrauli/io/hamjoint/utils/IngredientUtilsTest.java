package obrauli.io.hamjoint.utils;

import obrauli.io.hamjoint.data.models.Ingredient;
import obrauli.io.hamjoint.enums.IngredientType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class IngredientUtilsTest {
    @Test
    public void isBaconMustReturnTrueOnBacon() {
        Ingredient i = new Ingredient();
        i.setIngredientType(IngredientType.BACON);
        Assertions.assertTrue(IngredientUtils.isBacon(i));
    }

    @Test
    public void isBaconMustReturnFalseOnNotBacon() {
        Ingredient i = new Ingredient();
        i.setIngredientType(IngredientType.DAIRY_CHEESE);
        Assertions.assertFalse(IngredientUtils.isBacon(i));
    }

    @Test
    public void isBaconMustReturnFalseOnNullType() {
        Assertions.assertFalse(IngredientUtils.isBacon(new Ingredient()));
    }

    @Test
    public void isBaconMustReturnFalseOnNullObject() {
        Assertions.assertFalse(IngredientUtils.isBacon(null));
    }

    @Test
    public void isCheeseMustReturnTrueOnCheese() {
        Ingredient i = new Ingredient();
        i.setIngredientType(IngredientType.DAIRY_CHEESE);
        Assertions.assertTrue(IngredientUtils.isCheese(i));
    }

    @Test
    public void isCheeseMustReturnFalseOnNotCheese() {
        Ingredient i = new Ingredient();
        i.setIngredientType(IngredientType.CHICKEN_MEAT);
        Assertions.assertFalse(IngredientUtils.isCheese(i));
    }

    @Test
    public void isCheeseMustReturnFalseNullType() {
        Assertions.assertFalse(IngredientUtils.isCheese(new Ingredient()));
    }

    @Test
    public void isCheeseMustReturnFalseNullObject() {
        Assertions.assertFalse(IngredientUtils.isCheese(null));
    }


}
