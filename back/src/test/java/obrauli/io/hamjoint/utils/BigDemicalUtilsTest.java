package obrauli.io.hamjoint.utils;

import obrauli.io.hamjoint.exceptions.SelfAssertionException;
import org.assertj.core.api.Assertions;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class BigDemicalUtilsTest {

    @Test
    public void roundShouldReturn2On2_005(){
        Assertions.assertThat(BigDecimalUtils.round(new BigDecimal("2.005")))
                .isEqualByComparingTo(new BigDecimal(2));
    }

    @Test
    public void roundShouldReturn2On2(){
        Assertions.assertThat(BigDecimalUtils.round(new BigDecimal("2")))
                .isEqualByComparingTo(new BigDecimal(2));
    }

    @Test
    public void calculateDiscountShouldReturn10On10And9(){
        Assertions.assertThat(BigDecimalUtils.calculateDiscount(new BigDecimal("10"), new BigDecimal("9")))
                .isEqualByComparingTo(new BigDecimal("0.10"));
    }

    @Test
    public void calculateDiscountShouldReturn0On10And10(){
        Assertions.assertThat(BigDecimalUtils.calculateDiscount(new BigDecimal("10"), new BigDecimal("10")))
                .isEqualByComparingTo(BigDecimal.ZERO);
    }

    @Test
    public void calculateDiscountShouldReturn100On10And0(){
        Assertions.assertThat(BigDecimalUtils.calculateDiscount(new BigDecimal("10"), new BigDecimal("0")))
                .isEqualByComparingTo(BigDecimal.ONE);
    }

    @Test
    public void calculateDiscountShouldFailOn0And10(){
        org.junit.jupiter.api.Assertions.assertThrows(Throwable.class, () -> {
            BigDecimalUtils.calculateDiscount(new BigDecimal("0"), new BigDecimal("10"));
        });
    }

    @Test
    public void hasThreeOrMoreShouldReturnTrueIf3() {
        Assertions.assertThat(BigDecimalUtils.hasThreeOrMore(new BigDecimal("3"))).isTrue();
    }

    @Test
    public void hasThreeOrMoreShouldReturnTrueIf100() {
        Assertions.assertThat(BigDecimalUtils.hasThreeOrMore(new BigDecimal("100"))).isTrue();
    }

    @Test
    public void hasThreeOrMoreShouldReturnFalseIf2_99() {
        Assertions.assertThat(BigDecimalUtils.hasThreeOrMore(new BigDecimal("2.99"))).isFalse();
    }

    @Test
    public void hasThreeOrMoreShouldReturnFalseIf0() {
        Assertions.assertThat(BigDecimalUtils.hasThreeOrMore(BigDecimal.ZERO)).isFalse();
    }

    @Test
    public void hasThreeOrMoreShouldReturnFalseIfNegative100() {
        Assertions.assertThat(BigDecimalUtils.hasThreeOrMore(new BigDecimal("-100"))).isFalse();
    }

    @Test
    public void applyDiscountThreeForTwoShouldReturn3IfQuantityIs1AndPriceIs3() {
        Assertions
                .assertThat(BigDecimalUtils.applyDiscountThreeForTwo(BigDecimal.ONE, BigDecimalUtils.THREE))
                .isEqualTo(BigDecimalUtils.THREE);
    }

    @Test
    public void applyDiscountThreeForTwoShouldReturn3IfQuantityIs2AndPriceIs3() {
        Assertions
                .assertThat(BigDecimalUtils.applyDiscountThreeForTwo(BigDecimalUtils.TWO, BigDecimalUtils.THREE))
                .isEqualTo(BigDecimalUtils.THREE);
    }

    @Test
    public void applyDiscountThreeForTwoShouldReturn2IfQuantityIs3AndPriceIs3() {
        Assertions
                .assertThat(BigDecimalUtils.applyDiscountThreeForTwo(BigDecimalUtils.THREE, BigDecimalUtils.THREE))
                .isEqualTo(new BigDecimal("2.00"));
    }

    @Test
    public void applyDiscountThreeForTwoShouldReturn2IfQuantityIs6AndPriceIs3() {
        Assertions
                .assertThat(BigDecimalUtils.applyDiscountThreeForTwo(new BigDecimal("6.0"), BigDecimalUtils.THREE))
                .isEqualTo(new BigDecimal("2.00"));
    }

    @Test
    public void applyDiscountThreeForTwoShouldFailIfQuantityIsNull() {
        org.junit.jupiter.api.Assertions.assertThrows(SelfAssertionException.class, () -> {
            BigDecimalUtils.applyDiscountThreeForTwo(null, BigDecimalUtils.THREE);
        });
    }

    @Test
    public void applyDiscountThreeForTwoShouldFailIfPriceIsNull() {
        org.junit.jupiter.api.Assertions.assertThrows(SelfAssertionException.class, () -> {
            BigDecimalUtils.applyDiscountThreeForTwo(new BigDecimal("6.0"), null);
        });
    }

    @Test
    public void applyDiscountThreeForTwoShouldFailIfBothInputsAreNull() {
        org.junit.jupiter.api.Assertions.assertThrows(SelfAssertionException.class, () -> {
            BigDecimalUtils.applyDiscountThreeForTwo(null, null);
        });
    }

    @Test
    public void apply10PercentDiscountShouldReturn0_9On1() {
        Assertions.assertThat(BigDecimalUtils.apply10PercentDiscount(BigDecimal.ONE))
                .isEqualTo(new BigDecimal("0.9"));
    }

    @Test
    public void apply10PercentDiscountShouldReturn9On10() {
        Assertions.assertThat(BigDecimalUtils.apply10PercentDiscount(new BigDecimal(10)))
                .isEqualTo(new BigDecimal("9.0"));
    }

    @Test
    public void apply10PercentDiscountShouldFailOnNull() {
        org.junit.jupiter.api.Assertions.assertThrows(SelfAssertionException.class, () -> {
            BigDecimalUtils.apply10PercentDiscount(null);
        });
    }




}
