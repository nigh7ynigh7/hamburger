package obrauli.io.hamjoint.utils;

import obrauli.io.hamjoint.exceptions.SelfAssertionException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SelfAssertionTests {
    @Test
    public void shouldNotThrowOnTrue(){
        SelfAssertion.Assert(true);
    }

    @Test()
    public void shouldThrowOnFalse() {
        Assertions.assertThrows(SelfAssertionException.class, () -> {
            SelfAssertion.Assert(false);
        });
    }

    @Test
    public void shouldHaveMessageOnThrow() {
        final String message = "failed assertion";
        try {
            SelfAssertion.Assert(false, message);
        }
        catch (SelfAssertionException e) {
            message.equals(e.getMessage());
        }
    }

}
