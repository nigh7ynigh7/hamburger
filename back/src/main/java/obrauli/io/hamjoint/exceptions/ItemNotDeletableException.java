package obrauli.io.hamjoint.exceptions;

public class ItemNotDeletableException extends RuntimeException{
    public ItemNotDeletableException(String message) {
        super(message);
    }

    public ItemNotDeletableException(String message, Throwable cause) {
        super(message, cause);
    }
}
