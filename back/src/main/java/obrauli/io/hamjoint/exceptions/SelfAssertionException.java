package obrauli.io.hamjoint.exceptions;

public class SelfAssertionException extends RuntimeException {
    public SelfAssertionException(String message) {
        super(message);
    }

    public SelfAssertionException(String message, Throwable cause) {
        super(message, cause);
    }
}
