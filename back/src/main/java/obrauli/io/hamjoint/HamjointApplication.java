package obrauli.io.hamjoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableJpaRepositories(basePackages = { "obrauli.io.hamjoint.data.repositories" })
@EntityScan(basePackages = { "obrauli.io.hamjoint.data.models" })
@ComponentScan
@EnableTransactionManagement
@EnableAutoConfiguration
public class HamjointApplication {

	public static void main(String[] args) {
		SpringApplication.run(HamjointApplication.class, args);
	}

}
