package obrauli.io.hamjoint.filters;

import obrauli.io.hamjoint.filters.order.FilterOrder;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(value = FilterOrder.ShowDeletedFilter)
public class ShowDeletedFilter extends OncePerRequestFilter {

    public static InheritableThreadLocal<Boolean> SHOW_DELETED = new InheritableThreadLocal<>();

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        if("true".equals(httpServletRequest.getHeader("X-SHOW-DELETED"))) {
            SHOW_DELETED.set(true);;
        }
        else {
            SHOW_DELETED.set(false);
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
