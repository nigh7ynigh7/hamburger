package obrauli.io.hamjoint.filters.order;

public class FilterOrder {

    private FilterOrder(){}

    public static final int CorsConfiguration = 0;
    public static final int ShowDeletedFilter = CorsConfiguration + 1;
}
