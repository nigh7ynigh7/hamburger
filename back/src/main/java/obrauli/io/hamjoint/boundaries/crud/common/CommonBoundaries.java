package obrauli.io.hamjoint.boundaries.crud.common;

import obrauli.io.hamjoint.data.models.structures.Identifiable;
import obrauli.io.hamjoint.data.services.crud.common.CommonAbstractService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;

public abstract class CommonBoundaries
        <Id extends Serializable, Y extends Identifiable<Id>,  T extends JpaRepository<Y, Id>, S extends CommonAbstractService<Id, Y, T>> {

    private S service;

    public CommonBoundaries(S service) {
        this.service = service;
    }

    public S getService() {
        return this.service;
    }

    @GetMapping("/{id}")
    public @ResponseBody Y findById(@PathVariable("id") Id id) {
        return this.service.findById(id);
    }

    @DeleteMapping("/{id}")
    public @ResponseBody Y delete(@PathVariable("id") Id id) {
        return this.service.delete(id);
    }

    @PostMapping
    public @ResponseBody Y save(@RequestBody Y body) {
        body.setId(null);
        return this.service.save(body);
    }

    @PutMapping("/{id}")
    public @ResponseBody Y update(@PathVariable("id") Id id,
                                  @RequestBody Y body) {
        body.setId(id);
        return this.service.save(body);
    }

    @GetMapping
    public @ResponseBody Page<Y> getPaged(@RequestParam(value = "query", required = false) String query,
                                          @RequestParam(value = "size", defaultValue = "50") Integer size,
                                          @RequestParam(value = "page", defaultValue = "0") Integer page) {
        return this.service.getPagedSearch(query, Pageable.ofSize(size).withPage(page));
    }
}
