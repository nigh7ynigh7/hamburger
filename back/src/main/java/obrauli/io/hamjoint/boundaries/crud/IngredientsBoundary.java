package obrauli.io.hamjoint.boundaries.crud;

import obrauli.io.hamjoint.boundaries.crud.common.CommonBoundaries;
import obrauli.io.hamjoint.boundaries.crud.constants.PathConstants;
import obrauli.io.hamjoint.data.models.Ingredient;
import obrauli.io.hamjoint.data.repositories.IngredientRepository;
import obrauli.io.hamjoint.data.services.crud.IngredientService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(PathConstants.INGREDIENTS)
public class IngredientsBoundary extends CommonBoundaries<Long, Ingredient, IngredientRepository, IngredientService> {
    public IngredientsBoundary(IngredientService service) {
        super(service);
    }
}
