package obrauli.io.hamjoint.boundaries.crud.constants;

public class PathConstants {

    private PathConstants() {};

    private static final String root = "crud";
    public static final String INGREDIENTS = root + "/ingredients";
    public static final String HAMBURGER = root + "/hamburger";
    public static final String SIMPLE_ORDER = root + "/orders";
    public static final String INGREDIENTS_PER_ORDER = root + "/order-ingredients";
    public static final String HAMBURGER_INGREDIENT = root + "/hamburger-ingredient";

    private static final String EROOT = "types";
    public static final String INGREDIENT_TYPES = EROOT + "/ingredient-types";
    public static final String ONGOING_PROMOS = EROOT + "/ongoing-promos";

}
