package obrauli.io.hamjoint.boundaries.crud;


import obrauli.io.hamjoint.boundaries.crud.common.CommonBoundaries;
import obrauli.io.hamjoint.boundaries.crud.constants.PathConstants;
import obrauli.io.hamjoint.data.models.IngredientPerOrder;
import obrauli.io.hamjoint.data.repositories.IngredientPerOrderRepository;
import obrauli.io.hamjoint.data.services.crud.IngredientPerOrderService;
import obrauli.io.hamjoint.exceptions.NotAllowedException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(PathConstants.INGREDIENTS_PER_ORDER)
public class IngredientsPerOrderBoundary
        extends CommonBoundaries<Long, IngredientPerOrder, IngredientPerOrderRepository, IngredientPerOrderService> {
    public IngredientsPerOrderBoundary(IngredientPerOrderService service) {
        super(service);
    }

    @Override
    public IngredientPerOrder delete(Long aLong) {
        throw new NotAllowedException("manual access to this resource is prohibited");
    }

    @Override
    public IngredientPerOrder save(IngredientPerOrder body) {
        throw new NotAllowedException("manual access to this resource is prohibited");
    }

    @Override
    public IngredientPerOrder update(Long aLong, IngredientPerOrder body) {
        throw new NotAllowedException("manual access to this resource is prohibited");
    }
}
