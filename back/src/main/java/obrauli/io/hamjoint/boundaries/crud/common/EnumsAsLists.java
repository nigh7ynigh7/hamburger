package obrauli.io.hamjoint.boundaries.crud.common;

import obrauli.io.hamjoint.utils.SelfAssertion;
import org.springframework.web.bind.annotation.GetMapping;

import java.lang.reflect.Field;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class EnumsAsLists <T extends Enum<T>>  {

    public abstract Class<T> getEnumClass();

    @GetMapping
    public Set<String> getEnumData(){
        SelfAssertion.Assert(this.getEnumClass() != null, "reference class is null");
        return Stream.of(getEnumClass().getFields()).map(Field::getName).collect(Collectors.toSet());
    }
}
