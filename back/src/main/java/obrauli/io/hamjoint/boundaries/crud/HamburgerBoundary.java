package obrauli.io.hamjoint.boundaries.crud;

import obrauli.io.hamjoint.boundaries.crud.common.CommonBoundaries;
import obrauli.io.hamjoint.boundaries.crud.constants.PathConstants;
import obrauli.io.hamjoint.data.models.Hamburger;
import obrauli.io.hamjoint.data.repositories.HamburgerRepository;
import obrauli.io.hamjoint.data.services.crud.HamburgerService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(PathConstants.HAMBURGER)
public class HamburgerBoundary extends CommonBoundaries<Long, Hamburger, HamburgerRepository, HamburgerService> {
    public HamburgerBoundary(HamburgerService service) {
        super(service);
    }
}
