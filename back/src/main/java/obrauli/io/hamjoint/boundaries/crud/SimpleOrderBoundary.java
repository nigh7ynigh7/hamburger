package obrauli.io.hamjoint.boundaries.crud;

import obrauli.io.hamjoint.boundaries.crud.common.CommonBoundaries;
import obrauli.io.hamjoint.boundaries.crud.constants.PathConstants;
import obrauli.io.hamjoint.data.models.SimpleClientRequest;
import obrauli.io.hamjoint.data.repositories.UserOrderRepository;
import obrauli.io.hamjoint.data.services.crud.SimpleOrderService;
import obrauli.io.hamjoint.exceptions.NotAllowedException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(PathConstants.SIMPLE_ORDER)
public class SimpleOrderBoundary extends CommonBoundaries<Long, SimpleClientRequest, UserOrderRepository, SimpleOrderService> {
    public SimpleOrderBoundary(SimpleOrderService service) {
        super(service);
    }

    @Override
    public SimpleClientRequest delete(Long aLong) {
        throw new NotAllowedException("manual access to this resource is prohibited");
    }

    @Override
    public SimpleClientRequest save(SimpleClientRequest body) {
        throw new NotAllowedException("manual access to this resource is prohibited");
    }

    @Override
    public SimpleClientRequest update(Long aLong, SimpleClientRequest body) {
        throw new NotAllowedException("manual access to this resource is prohibited");
    }
}
