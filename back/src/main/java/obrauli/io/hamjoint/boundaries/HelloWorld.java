package obrauli.io.hamjoint.boundaries;

import obrauli.io.hamjoint.constants.ExecutionConstants;
import obrauli.io.hamjoint.utils.AppDateFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("")
public class HelloWorld {

    @GetMapping
    public @ResponseBody Map<String, Object> hello() {
        Map<String, Object> output = new HashMap<>();
        output.put("message", "Welcome to the hamburger api, we make hamburgers here.");
        output.put("upSince", AppDateFormat.toDateString(ExecutionConstants.UP_SINCE));
        return output;
    }

    @GetMapping("/health-check")
    public @ResponseBody Boolean healthCheck() {
        return true;
    }
}
