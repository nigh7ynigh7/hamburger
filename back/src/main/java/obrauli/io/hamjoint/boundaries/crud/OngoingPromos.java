package obrauli.io.hamjoint.boundaries.crud;

import obrauli.io.hamjoint.boundaries.crud.common.EnumsAsLists;
import obrauli.io.hamjoint.boundaries.crud.constants.PathConstants;
import obrauli.io.hamjoint.enums.DiscountTypes;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(PathConstants.ONGOING_PROMOS)
public class OngoingPromos extends EnumsAsLists<DiscountTypes> {
    @Override
    public Class<DiscountTypes> getEnumClass() {
        return DiscountTypes.class;
    }
}
