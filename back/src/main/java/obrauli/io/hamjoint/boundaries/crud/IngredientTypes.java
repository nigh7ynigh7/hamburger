package obrauli.io.hamjoint.boundaries.crud;

import obrauli.io.hamjoint.boundaries.crud.common.EnumsAsLists;
import obrauli.io.hamjoint.boundaries.crud.constants.PathConstants;
import obrauli.io.hamjoint.enums.IngredientType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(PathConstants.INGREDIENT_TYPES)
public class IngredientTypes extends EnumsAsLists<IngredientType> {
    @Override
    public Class<IngredientType> getEnumClass() {
        return IngredientType.class;
    }
}
