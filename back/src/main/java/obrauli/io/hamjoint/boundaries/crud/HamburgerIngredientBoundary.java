package obrauli.io.hamjoint.boundaries.crud;


import obrauli.io.hamjoint.boundaries.crud.common.CommonBoundaries;
import obrauli.io.hamjoint.boundaries.crud.constants.PathConstants;
import obrauli.io.hamjoint.data.models.HamburgerIngredient;
import obrauli.io.hamjoint.data.repositories.HamburgerIngredientRepository;
import obrauli.io.hamjoint.data.services.crud.HamburgerIngredientService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(PathConstants.HAMBURGER_INGREDIENT)
public class HamburgerIngredientBoundary
        extends CommonBoundaries<Long, HamburgerIngredient, HamburgerIngredientRepository, HamburgerIngredientService> {
    public HamburgerIngredientBoundary(HamburgerIngredientService service) {
        super(service);
    }
}
