package obrauli.io.hamjoint.boundaries;

import obrauli.io.hamjoint.data.dto.CompactUserRequest;
import obrauli.io.hamjoint.data.models.SimpleClientRequest;
import obrauli.io.hamjoint.data.services.OrderService;
import org.springframework.web.bind.annotation.*;

@RestController
public class OrderBoundary {

    private final OrderService orderService;

    public OrderBoundary(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/place-order")
    public @ResponseBody String placeOrder(@RequestBody CompactUserRequest request) {
        return this.orderService.makeOrder(request);
    }

    @GetMapping("/your-order/{uuid}")
    public @ResponseBody SimpleClientRequest getDetailedOrder(@PathVariable("uuid") String uuid) {
        return this.orderService.fetchDetailedRequest(uuid);
    }
}
