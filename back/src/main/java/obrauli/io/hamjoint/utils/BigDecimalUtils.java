package obrauli.io.hamjoint.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BigDecimalUtils {
    public static final BigDecimal THREE = new BigDecimal(3);
    public static final BigDecimal TWO = new BigDecimal(2);
    public static final BigDecimal ONE_TENTH = new BigDecimal("0.1");
    
    public static BigDecimal round(BigDecimal b) {
        SelfAssertion.Assert(b != null);
        return b.setScale(2, RoundingMode.DOWN);
    }

    public static BigDecimal calculateDiscount(BigDecimal bigger, BigDecimal smaller) {
        SelfAssertion.Assert(bigger != null);
        SelfAssertion.Assert(smaller != null);
        return BigDecimal.ONE.subtract(smaller.divide(bigger, 2, RoundingMode.DOWN));
    }
    
    public static boolean hasThreeOrMore(BigDecimal b) {
        SelfAssertion.Assert(b != null);
        return THREE.compareTo(b) <= 0;
    }
    
    public static BigDecimal applyDiscountThreeForTwo(BigDecimal quantity, BigDecimal price){

        SelfAssertion.Assert(price != null);
        SelfAssertion.Assert(quantity != null);

        if(hasThreeOrMore(quantity)) {
            BigDecimal result = quantity.divide(THREE, 0, RoundingMode.FLOOR);
            BigDecimal remainder = quantity.remainder(THREE);
            return result.multiply(TWO).add(remainder).multiply(price).divide(quantity, 2, RoundingMode.DOWN);
        }
        return price;
    }

    public static BigDecimal apply10PercentDiscount(BigDecimal price){
        SelfAssertion.Assert(price != null);
        return price.multiply(BigDecimal.ONE.subtract(ONE_TENTH));
    }
}
