package obrauli.io.hamjoint.utils;

import obrauli.io.hamjoint.exceptions.SelfAssertionException;

public class SelfAssertion {
    public static void Assert(boolean condition, String onError) {
        if(!condition) {
            throw new SelfAssertionException(onError == null || onError.isBlank() ? "assertion failed" : onError);
        }
    }
    public static void Assert(boolean condition) {
        Assert(condition, null);
    }
}
