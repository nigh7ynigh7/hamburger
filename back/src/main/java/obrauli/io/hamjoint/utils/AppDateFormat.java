package obrauli.io.hamjoint.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AppDateFormat {

    private static final Logger LOG = LoggerFactory.getLogger(AppDateFormat.class);

    private static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");

    public static String toDateString(Date date) {
        if(date == null) return null;
        return format.format(date);
    }

    public static Date fromStringDate(String s) {
        try {
            SelfAssertion.Assert(s != null && !s.isBlank(), "Empty date");
            return format.parse(s);
        }
        catch (Throwable t) {
            LOG.error("Could not convert date",t);
            return null;
        }
    }
}
