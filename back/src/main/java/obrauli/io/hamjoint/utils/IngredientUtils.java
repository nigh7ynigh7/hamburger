package obrauli.io.hamjoint.utils;

import obrauli.io.hamjoint.data.models.Ingredient;
import obrauli.io.hamjoint.enums.IngredientType;

import java.util.stream.Stream;

public class IngredientUtils {

    public static boolean isBacon(Ingredient i) {
        if(i == null) {
            return false;
        }
        return Stream.of(IngredientType.BACON).anyMatch(type -> type.equals(i.getIngredientType()));
    }

    public static boolean isCheese(Ingredient i) {
        if(i == null) {
            return false;
        }
        return Stream.of(IngredientType.DAIRY_CHEESE).anyMatch(type -> type.equals(i.getIngredientType()));
    }
}
