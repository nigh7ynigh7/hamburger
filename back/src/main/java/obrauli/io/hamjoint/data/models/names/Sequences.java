package obrauli.io.hamjoint.data.models.names;

public class Sequences {
    public static final String INGREDIENTS_ID_SQ = "SEQ_" + Tables.INGREDIENTS;
    public static final String HAMBURGER_ID_SQ = "SEQ_" + Tables.HAMBURGER;
    public static final String HAMBURGER_INGREDIENT_ID_SQ = "SEQ_" + Tables.INGREDIENTS_HAMBURGER;
    public static final String INGREDIENT_PER_ORDER_SQ = "SEQ_" + Tables.INGREDIENT_PER_ORDER;
    public static final String SIMPLE_CLIENT_ORDER_SQ = "SEQ_" + Tables.CLIENT_REQUEST;
}
