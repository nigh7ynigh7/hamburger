package obrauli.io.hamjoint.data.models.names;

public class Fields {
    public static final String NAME = "NAME";
    public static final String ID = "ID";
    public static final String CREATED_AT = "CREATED_AT";
    public static final String UPDATED_AT = "UPDATED_AT";
    public static final String QUANTITY = "QUANTITY";
    public static final String VALUE = "VALUE";
    public static final String ACTIVE = "ACTIVE";
    public static final String FINAL_TOTAL = "FINAL_TOTAL";
    public static final String FRACTIONAL = "FRACTIONAL";
    public static final String PRICING_AT_TIME = "PRICING_AT_TIME";
    public static final String FINAL_COST = "FINAL_COST";
    public static final String DISCOUNT = "DISCOUNT";
    public static final String DESCRIPTION = "DESCRIPTION";
    public static final String INGREDIENT_TYPE = "INGREDIENT_TYPE";

    public static final String HAMBURGER_ID = Tables.HAMBURGER + "_" + ID;
    public static final String SIMPLE_ORDER_ID = Tables.CLIENT_REQUEST + "_" + ID;
    public static final String INGREDIENT_ID = Tables.INGREDIENTS + "_" + ID;

    public static final String UNIQUE_IDENTIFIER = "UNIQUE_IDENTIFIER";
}