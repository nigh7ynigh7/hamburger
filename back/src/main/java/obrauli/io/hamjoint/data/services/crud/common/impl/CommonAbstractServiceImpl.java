package obrauli.io.hamjoint.data.services.crud.common.impl;

import obrauli.io.hamjoint.data.models.structures.Changeable;
import obrauli.io.hamjoint.data.models.structures.Deletable;
import obrauli.io.hamjoint.data.models.structures.Identifiable;
import obrauli.io.hamjoint.data.repositories.presets.DeletableRepositories;
import obrauli.io.hamjoint.data.repositories.presets.NamedDeletableRepositories;
import obrauli.io.hamjoint.data.repositories.presets.NamedRepositories;
import obrauli.io.hamjoint.data.reference.repository.RootRepository;
import obrauli.io.hamjoint.data.services.crud.common.CommonAbstractService;
import obrauli.io.hamjoint.exceptions.ItemNotDeletableException;
import obrauli.io.hamjoint.exceptions.RegistryNotFoundException;
import obrauli.io.hamjoint.filters.ShowDeletedFilter;
import obrauli.io.hamjoint.utils.SelfAssertion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class CommonAbstractServiceImpl
        <Id extends Serializable, Y extends Identifiable<Id>,  T extends RootRepository<Y, Id>>
        implements CommonAbstractService<Id, Y, T> {

    private T repository = null;

    public CommonAbstractServiceImpl(T repository) {
        if(repository == null) {
            throw new RuntimeException("Incoming repository cannot be null");
        }
        this.repository = repository;
    }

    @Override
    public T getRepository() {
        return this.repository;
    }

    @Override
    @SuppressWarnings({ "unchecked "})
    public Page<Y> getPagedSearch(String query, Pageable pageable) {
        // after it all works, switch to querydsl
        Set<Boolean> which = new HashSet<>();
        which.add(true);
        if(Boolean.TRUE.equals(ShowDeletedFilter.SHOW_DELETED.get())){
            which.add(false);
        }
        query = query == null ? "" : query;
        if(this.repository instanceof NamedDeletableRepositories) {
            return ((NamedDeletableRepositories) this.repository).findByNameIgnoreCaseContainingAndActiveIsIn(query, pageable, which);
        }
        if(this.repository instanceof NamedRepositories) {
            return ((NamedRepositories) this.repository).findByNameIgnoreCaseContaining(query, pageable);
        }
        if(this.repository instanceof DeletableRepositories) {
            ((DeletableRepositories<?>) this.repository).findByActiveIsIn(pageable, which);
        }

        this.getLogger().info("with the given types, this collection cannot be queried. defaulting simple pagination");
        return this.repository.findAll(pageable);
    }

    @Override
    public Y save(Y incoming) {
        SelfAssertion.Assert(incoming != null, "No assertion");
        Y toSave = incoming;

        if(toSave.getId() != null) {
            toSave = this.findById(toSave.getId());
        }

        this.applyOnUpdate(toSave, incoming);
        this.validate(toSave);
        this.postValidateUpdate(toSave);

        if(toSave instanceof Changeable) {
            Date date = new Date();
            if(incoming == toSave){
                ((Changeable) toSave).setCreatedAt(date);
            }
            ((Changeable) toSave).setUpdatedAt(date);
        }

        Y saved = this.repository.save(toSave);
        this.postSaveApply(saved, toSave);
        this.afterSaved(saved);

        return saved;
    }

    @Override
    public void postSaveApply(Y saved, Y toSave) {
    }

    @Override
    public void afterSaved(Y saved) {

    }

    @Override
    public Y findById(Id id) {
        var output = this.repository.findById(id);
        output.ifPresent(i -> {
            if(i instanceof Deletable
                    && Boolean.FALSE.equals(((Deletable) i).getActive())
                    && !Boolean.TRUE.equals(ShowDeletedFilter.SHOW_DELETED.get())) {
                throw new RegistryNotFoundException("registry not found or deleted");
            }
        });
        return output.orElseThrow(() -> new RegistryNotFoundException("registry not found"));
    }

    @Override
    public Y delete(Id id) {
        SelfAssertion.Assert(id != null, "id cannot be null for this operation");
        Y found = this.findById(id);
        if(found instanceof Deletable){
            ((Deletable) found).setActive(false);
            if(found instanceof Changeable) {
                ((Changeable) found).setUpdatedAt(new Date());
            }
            this.repository.save(found);
            return found;
        }
        else {
            throw new ItemNotDeletableException("item not deletable");
        }
    }

    @Override
    public Y delete(Y incoming) {
        return this.delete(incoming != null ? incoming.getId() : null);
    }

    @Override
    public List<Y> getAllItems() {
        return this.repository.findAll();
    }

    @Override
    public List<Y> findByIdIn(Set<Id> collect) {
        return this.repository.findAllByIdIn(collect);
    }

    @Override
    public Logger getLogger() {
        return LoggerFactory.getLogger(this.getClass());
    }
}
