package obrauli.io.hamjoint.data.models.names;

public class Constraints {
    public static final String INGREDIENT_ORDER_UNIQUE_CONSTRAINT = "UNIQUE_INGREDIENT_ORDER";
    public static final String INGREDIENT_HAMBURGER_UNIQUE_CONSTRAINT = "UNIQUE_HAMBURGER_INGREDIENT_CONSTRAINT";
}
