package obrauli.io.hamjoint.data.services.impl;

import obrauli.io.hamjoint.components.StaticApplicationContext;
import obrauli.io.hamjoint.data.dto.CompactUserRequest;
import obrauli.io.hamjoint.data.dto.DiscountStep;
import obrauli.io.hamjoint.data.dto.IngredientCount;
import obrauli.io.hamjoint.data.dto.IngredientPrice;
import obrauli.io.hamjoint.data.models.Hamburger;
import obrauli.io.hamjoint.data.models.Ingredient;
import obrauli.io.hamjoint.data.models.IngredientPerOrder;
import obrauli.io.hamjoint.data.models.SimpleClientRequest;
import obrauli.io.hamjoint.data.services.OrderService;
import obrauli.io.hamjoint.data.services.crud.HamburgerService;
import obrauli.io.hamjoint.data.services.crud.IngredientService;
import obrauli.io.hamjoint.data.services.crud.SimpleOrderService;
import obrauli.io.hamjoint.discount.DiscountProcessors;
import obrauli.io.hamjoint.utils.BigDecimalUtils;
import obrauli.io.hamjoint.utils.SelfAssertion;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    private final HamburgerService hamburgerService;
    private final IngredientService ingredientService;
    private final SimpleOrderService simpleOrderService;
    private final List<DiscountProcessors> discountProcessors;

    public OrderServiceImpl(HamburgerService hamburgerService,
                            IngredientService ingredientService,
                            SimpleOrderService simpleOrderService,
                            List<DiscountProcessors> discountProcessors){
        this.hamburgerService = hamburgerService;
        this.ingredientService = ingredientService;
        this.simpleOrderService = simpleOrderService;
        if(discountProcessors != null && discountProcessors.size() > 0) {
            this.discountProcessors = discountProcessors.stream().sorted((a, b) -> a.getWeight() > b.getWeight() ? -1 : 0).collect(Collectors.toList());
        }
        else {
            this.discountProcessors = Collections.emptyList();
        }
    }

    @Override
    @Transactional(rollbackFor = {Throwable.class, RuntimeException.class})
    public String makeOrder(CompactUserRequest userRequest) {

        validateUserRequest(userRequest);

        // fetches the burger in question
        Hamburger hamburger = this.hamburgerService.findById(userRequest.getHamburgerId());

        DiscountStep first = buildFirstStep(userRequest, hamburger);

        applyInPlaceFilterOnStep(first);

        secondaryStepValidationAndInitialization(first);

        List<DiscountStep> list = applyDiscountsAndGenerateSteps(first);

        return persist(userRequest, hamburger, list);
    }

    String persist(CompactUserRequest userRequest, Hamburger hamburger, List<DiscountStep> list) {
        DiscountStep lastStep = list.get(list.size() - 1);

        // build the object to save
        SimpleClientRequest output = new SimpleClientRequest();
        output.setName(userRequest.getForWhom());
        output.setDescription(userRequest.getDescription());
        output.setSelectedHamburger(hamburger);
        output.setTotal(list.get(0).getCalculatedTotal());
        output.setFinalTotal(list.get(list.size() - 1).getCalculatedTotal());

        // apply final logic
        output.setIngredientPerOrders(lastStep.getIngredients().stream().map(item -> {
            Ingredient i = item.getIngredient();

            IngredientPerOrder ingredientPerOrder = new IngredientPerOrder();
            ingredientPerOrder.setIngredient(i);
            ingredientPerOrder.setDiscount(BigDecimalUtils.calculateDiscount(item.getStartPrice(), item.getEndPrice()));
            ingredientPerOrder.setFinalCost(item.getEndPrice().multiply(item.getQuantity()));
            ingredientPerOrder.setPricingAtTime(item.getIngredient().getValue());
            ingredientPerOrder.setWasFractional(ingredientPerOrder.getWasFractional());
            ingredientPerOrder.setQuantityCount(item.getQuantity());

            return ingredientPerOrder;
        }).collect(Collectors.toList()));

        // save object and return an uuid
        return this.simpleOrderService.save(output).getUuid();
    }

    List<DiscountStep> applyDiscountsAndGenerateSteps(DiscountStep first) {
        // starts the discount steps
        List<DiscountStep> list = new ArrayList<>();
        list.add(first);
        // step through each promotional phase attributing and stacking the discounts - doing it on a per-item basis
        this.discountProcessors
                .forEach(item -> {
                    DiscountStep last = item.applyDiscount(list);
                    if(last != null) {
                        list.add(last);
                    }
                });
        return list;
    }

    void secondaryStepValidationAndInitialization(DiscountStep first) {
        SelfAssertion.Assert(!ObjectUtils.isEmpty(first.getIngredients()), "resolved to no ingredients");

        // initializing values
        first.setStartingValue(first.getCalculatedTotal());
        first.setEndingValue(first.getStartingValue());
    }

    void applyInPlaceFilterOnStep(DiscountStep first) {
        // floors and quantities where fractional values are not allowed
        first.setIngredients(
                first.getIngredients()
                        .stream()
                        .peek(item -> {
                            if(!Boolean.TRUE.equals(item.getIngredient().getAllowFractional())) {
                                item.setQuantity(item.getQuantity().setScale(0, RoundingMode.FLOOR));
                            }
                        })
                        .filter(item -> item.getQuantity().compareTo(BigDecimal.ZERO) > 0)
                        .collect(Collectors.toSet()));
    }

    DiscountStep buildFirstStep(CompactUserRequest userRequest, Hamburger hamburger) {
        // build the first discount step, ignores anything the user asked to overwrite ...
        DiscountStep firstStep = new DiscountStep();

        Map<Long, IngredientCount> ids = userRequest
                .getOverwriteIngredients()
                .stream()
                .collect(Collectors.toMap(IngredientCount::getIngredientId, i -> i));

        Set<Long> keys = ids.keySet();

        // handles overwriting values
        ids.forEach((key, val) -> {
            // if zero is bigger
            if(BigDecimal.ZERO.compareTo(val.getQuantity()) > 0 ) return;

            // find ingredient and add it locally
            Ingredient ingredient = this.ingredientService.findById(key);
            if(Boolean.TRUE.equals(ingredient.getActive())){
                IngredientPrice i = new IngredientPrice();
                i.setIngredient(ingredient);
                i.setQuantity(val.getQuantity());
                BigDecimal fval = ingredient.getValue();
                i.setStartPrice(fval);
                i.setEndPrice(fval);
                firstStep.getIngredients().add(i);
            }
        });

        // handles additional values
        hamburger.getHamburgerIngredients()
                .stream()
                .filter(i -> !keys.contains(i.getIngredient().getId()))
                .forEach(item -> {
                    IngredientPrice i = new IngredientPrice();
                    i.setIngredient(item.getIngredient());
                    i.setQuantity(item.getQuantity());
                    BigDecimal val = item.getIngredient().getValue();
                    i.setStartPrice(val);
                    i.setEndPrice(val);
                    firstStep.getIngredients().add(i);
                });

        return firstStep;
    }

    void validateUserRequest(CompactUserRequest userRequest) {
        // some basic preliminary validations
        SelfAssertion.Assert(!ObjectUtils.isEmpty(userRequest.getForWhom()), "name required");
        SelfAssertion.Assert(!ObjectUtils.isEmpty(userRequest.getHamburgerId()) && userRequest.getHamburgerId() > 0, "no hamburger id");
        if(userRequest.getOverwriteIngredients() != null) {
            userRequest.getOverwriteIngredients().forEach(item -> {
                SelfAssertion.Assert(!ObjectUtils.isEmpty(item.getQuantity()), "no quantity");
                SelfAssertion.Assert(!ObjectUtils.isEmpty(item.getIngredientId()), "no ingredient");
                SelfAssertion.Assert(item.getQuantity().compareTo(BigDecimal.ZERO) >=0, "an ingredient smaller than 0 found");
            });
        }
    }

    @Override
    public SimpleClientRequest fetchDetailedRequest(String id) {
        SelfAssertion.Assert(!ObjectUtils.isEmpty(id), "no uuid");
        return this.simpleOrderService.findByUuid(id);
    }

}
