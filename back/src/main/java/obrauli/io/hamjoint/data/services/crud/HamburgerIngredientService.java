package obrauli.io.hamjoint.data.services.crud;

import obrauli.io.hamjoint.data.models.HamburgerIngredient;
import obrauli.io.hamjoint.data.repositories.HamburgerIngredientRepository;
import obrauli.io.hamjoint.data.services.crud.common.CommonAbstractService;

import java.util.Set;

public interface HamburgerIngredientService extends CommonAbstractService<Long, HamburgerIngredient, HamburgerIngredientRepository> {
    void removeAllByHamburgerId(Set<Long> ids);
}
