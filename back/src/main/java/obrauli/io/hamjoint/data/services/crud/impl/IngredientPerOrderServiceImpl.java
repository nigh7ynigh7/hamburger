package obrauli.io.hamjoint.data.services.crud.impl;

import obrauli.io.hamjoint.data.models.IngredientPerOrder;
import obrauli.io.hamjoint.data.repositories.IngredientPerOrderRepository;
import obrauli.io.hamjoint.data.services.crud.IngredientPerOrderService;
import obrauli.io.hamjoint.data.services.crud.common.impl.CommonAbstractServiceImpl;
import obrauli.io.hamjoint.utils.SelfAssertion;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class IngredientPerOrderServiceImpl
        extends CommonAbstractServiceImpl<Long, IngredientPerOrder, IngredientPerOrderRepository>
        implements IngredientPerOrderService {

    public IngredientPerOrderServiceImpl(IngredientPerOrderRepository repository) {
        super(repository);
    }

    @Override
    public void validate(IngredientPerOrder toSave) {
        SelfAssertion.Assert(toSave.getOrder() != null && toSave.getOrder().getId() != null, "no order for order");
        SelfAssertion.Assert(toSave.getIngredient() != null && toSave.getIngredient().getId() != null, "no ingredient for order");
        SelfAssertion.Assert(toSave.getWasFractional() != null, "must know if fraction");
        SelfAssertion.Assert(toSave.getDiscount() != null, "discount is required");
        SelfAssertion.Assert(toSave.getFinalCost() != null, "final cost id required");
        SelfAssertion.Assert(toSave.getPricingAtTime() != null, "pricing is required");
        SelfAssertion.Assert(toSave.getQuantityCount() != null, "quantity/count is required");
    }

    @Override
    public void applyOnUpdate(IngredientPerOrder toSave, IngredientPerOrder newOrUpdating) {}

    @Override
    public void postValidateUpdate(IngredientPerOrder toSave) {
    }

}
