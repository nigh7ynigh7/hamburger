package obrauli.io.hamjoint.data.services.crud;

import obrauli.io.hamjoint.data.models.Ingredient;
import obrauli.io.hamjoint.data.repositories.IngredientRepository;
import obrauli.io.hamjoint.data.services.crud.common.CommonAbstractService;

public interface IngredientService extends CommonAbstractService<Long, Ingredient, IngredientRepository> {
}
