package obrauli.io.hamjoint.data.services.crud;

import obrauli.io.hamjoint.data.models.SimpleClientRequest;
import obrauli.io.hamjoint.data.repositories.UserOrderRepository;
import obrauli.io.hamjoint.data.services.crud.common.CommonAbstractService;

public interface SimpleOrderService extends CommonAbstractService<Long, SimpleClientRequest, UserOrderRepository> {
    SimpleClientRequest findByUuid(String id);
}
