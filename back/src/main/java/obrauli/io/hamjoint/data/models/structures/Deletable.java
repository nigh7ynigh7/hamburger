package obrauli.io.hamjoint.data.models.structures;

public interface Deletable {
    void setActive(Boolean active);
    Boolean getActive();
}
