package obrauli.io.hamjoint.data.repositories;

import obrauli.io.hamjoint.data.models.SimpleClientRequest;
import obrauli.io.hamjoint.data.repositories.presets.NamedRepositories;
import obrauli.io.hamjoint.data.reference.repository.RootRepository;

import java.util.Optional;

public interface UserOrderRepository
        extends RootRepository<SimpleClientRequest, Long>, NamedRepositories<SimpleClientRequest> {
    Optional<SimpleClientRequest> findByUuid(String id);
}
