package obrauli.io.hamjoint.data.services;

import obrauli.io.hamjoint.data.dto.CompactUserRequest;
import obrauli.io.hamjoint.data.models.SimpleClientRequest;

public interface OrderService {
    String makeOrder(CompactUserRequest userRequest);
    SimpleClientRequest fetchDetailedRequest(String id);
}
