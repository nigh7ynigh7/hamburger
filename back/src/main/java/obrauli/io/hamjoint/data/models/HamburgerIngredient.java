package obrauli.io.hamjoint.data.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import obrauli.io.hamjoint.data.models.names.Constraints;
import obrauli.io.hamjoint.data.models.names.Fields;
import obrauli.io.hamjoint.data.models.names.Sequences;
import obrauli.io.hamjoint.data.models.names.Tables;
import obrauli.io.hamjoint.data.models.structures.Identifiable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(
        name = Tables.INGREDIENTS_HAMBURGER,
        uniqueConstraints =
        @UniqueConstraint(
                name = Constraints.INGREDIENT_HAMBURGER_UNIQUE_CONSTRAINT,
                columnNames = { Fields.HAMBURGER_ID, Fields.INGREDIENT_ID }
        )
)
public class HamburgerIngredient implements Identifiable<Long>, Serializable {

    @Id
    @Column(name = Fields.ID)
    @SequenceGenerator(
            name = Sequences.HAMBURGER_INGREDIENT_ID_SQ,
            sequenceName = Sequences.HAMBURGER_INGREDIENT_ID_SQ,
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = Sequences.HAMBURGER_INGREDIENT_ID_SQ
    )
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = Fields.HAMBURGER_ID, updatable = false, nullable = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Hamburger hamburger;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = Fields.INGREDIENT_ID, updatable = false, nullable = false)
    private Ingredient ingredient;

    @Column(name = Fields.QUANTITY, updatable = true)
    private BigDecimal quantity = BigDecimal.ZERO;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Hamburger getHamburger() {
        return hamburger;
    }

    public void setHamburger(Hamburger hamburger) {
        this.hamburger = hamburger;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }
}
