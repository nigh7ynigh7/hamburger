package obrauli.io.hamjoint.data.services.crud;

import obrauli.io.hamjoint.data.models.Hamburger;
import obrauli.io.hamjoint.data.models.SimpleClientRequest;
import obrauli.io.hamjoint.data.repositories.HamburgerRepository;
import obrauli.io.hamjoint.data.services.crud.common.CommonAbstractService;

public interface HamburgerService extends CommonAbstractService<Long, Hamburger, HamburgerRepository> {
}
