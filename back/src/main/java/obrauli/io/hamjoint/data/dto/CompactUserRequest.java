package obrauli.io.hamjoint.data.dto;

import java.util.ArrayList;
import java.util.List;

public class CompactUserRequest {
    private Long hamburgerId = 0L;
    private List<IngredientCount> overwriteIngredients = new ArrayList<>();
    private String forWhom = null;
    private String description = null;

    public Long getHamburgerId() {
        return hamburgerId;
    }

    public void setHamburgerId(Long hamburgerId) {
        this.hamburgerId = hamburgerId;
    }

    public List<IngredientCount> getOverwriteIngredients() {
        return overwriteIngredients;
    }

    public void setOverwriteIngredients(List<IngredientCount> overwriteIngredients) {
        this.overwriteIngredients = overwriteIngredients;
    }

    public String getForWhom() {
        return forWhom;
    }

    public void setForWhom(String forWhom) {
        this.forWhom = forWhom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
