package obrauli.io.hamjoint.data.repositories;

import obrauli.io.hamjoint.data.models.Ingredient;
import obrauli.io.hamjoint.data.repositories.presets.NamedDeletableRepositories;
import obrauli.io.hamjoint.data.reference.repository.RootRepository;

public interface IngredientRepository
        extends RootRepository<Ingredient, Long>, NamedDeletableRepositories<Ingredient> {
}
