package obrauli.io.hamjoint.data.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import obrauli.io.hamjoint.data.models.names.Fields;
import obrauli.io.hamjoint.data.models.names.Sequences;
import obrauli.io.hamjoint.data.models.names.Tables;
import obrauli.io.hamjoint.data.models.structures.Changeable;
import obrauli.io.hamjoint.data.models.structures.Deletable;
import obrauli.io.hamjoint.data.models.structures.Identifiable;
import obrauli.io.hamjoint.data.models.structures.Named;
import obrauli.io.hamjoint.enums.IngredientType;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = Tables.INGREDIENTS)
public class Ingredient implements Identifiable<Long>, Named, Changeable, Deletable, Serializable {

    public static final Boolean DEFAULT_FRACTIONAL = true;

    @Id
    @Column(name = Fields.ID)
    @SequenceGenerator(
            name = Sequences.INGREDIENTS_ID_SQ,
            sequenceName = Sequences.INGREDIENTS_ID_SQ,
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = Sequences.INGREDIENTS_ID_SQ
    )
    private Long id;

    @Column(nullable = false, name = Fields.NAME)
    private String name;

    @Column(nullable = false, name = Fields.VALUE)
    private BigDecimal value;

    @Column(name = Fields.UPDATED_AT)
    private Date updatedAt = new Date();

    @Column(nullable = false, name = Fields.CREATED_AT, updatable = false)
    private Date createdAt = new Date();

    @Column(nullable = false, name = Fields.ACTIVE)
    private Boolean active = true;

    @Column(name = Fields.FRACTIONAL)
    private Boolean allowFractional = DEFAULT_FRACTIONAL;

    @Column(nullable = false, name = Fields.INGREDIENT_TYPE)
    private IngredientType ingredientType = null;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @OneToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST }, mappedBy="ingredient", fetch = FetchType.LAZY)
    private Set<HamburgerIngredient> hamburgers;

    @Override
    public Boolean getActive() {
        return active;
    }

    @Override
    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    @Override
    public Date getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public Date getCreatedAt() {
        return createdAt;
    }

    @Override
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Set<HamburgerIngredient> getHamburgers() {
        return hamburgers;
    }

    public void setHamburgers(Set<HamburgerIngredient> hamburgers) {
        this.hamburgers = hamburgers;
    }

    public Boolean getAllowFractional() {
        return allowFractional;
    }

    public void setAllowFractional(Boolean allowFractional) {
        this.allowFractional = allowFractional;
    }

    public IngredientType getIngredientType() {
        return ingredientType;
    }

    public void setIngredientType(IngredientType ingredientType) {
        this.ingredientType = ingredientType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ingredient that = (Ingredient) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
