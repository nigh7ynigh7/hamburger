package obrauli.io.hamjoint.data.repositories;

import obrauli.io.hamjoint.data.models.Hamburger;
import obrauli.io.hamjoint.data.repositories.presets.NamedDeletableRepositories;
import obrauli.io.hamjoint.data.reference.repository.RootRepository;

import java.util.Optional;

public interface HamburgerRepository
        extends RootRepository<Hamburger, Long>, NamedDeletableRepositories<Hamburger> {
}
