package obrauli.io.hamjoint.data.services.crud;

import obrauli.io.hamjoint.data.models.IngredientPerOrder;
import obrauli.io.hamjoint.data.repositories.IngredientPerOrderRepository;
import obrauli.io.hamjoint.data.services.crud.common.CommonAbstractService;

public interface IngredientPerOrderService extends CommonAbstractService<Long, IngredientPerOrder, IngredientPerOrderRepository> {
}
