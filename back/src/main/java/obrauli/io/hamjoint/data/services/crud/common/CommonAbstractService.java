package obrauli.io.hamjoint.data.services.crud.common;

import obrauli.io.hamjoint.data.models.Ingredient;
import obrauli.io.hamjoint.data.models.structures.Identifiable;
import org.slf4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

public interface CommonAbstractService <Id extends Serializable, Y extends Identifiable<Id>,  T extends JpaRepository<Y, Id>> {

    Page<Y> getPagedSearch(String query, Pageable pageable);
    List<Y> getAllItems();

    void validate(Y toSave);
    void applyOnUpdate(Y toSave, Y newOrUpdating);
    void postValidateUpdate(Y toSave);

    Y save (Y incoming);

    Y findById(Id id);

    Y delete(Id id);
    Y delete(Y incoming);

    T getRepository();

    Logger getLogger();

    List<Y> findByIdIn(Set<Id> collect);

    void postSaveApply(Y saved, Y toSave);

    void afterSaved(Y saved);
}
