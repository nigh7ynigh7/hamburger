package obrauli.io.hamjoint.data.repositories.presets;

import obrauli.io.hamjoint.data.models.structures.Deletable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface DeletableRepositories <Y extends Deletable> {
    Optional<List<Y>> findByActiveIsIn(Set<Boolean> which);
    Page<Y> findByActiveIsIn(Pageable pageable, Set<Boolean> which);
}
