package obrauli.io.hamjoint.data.repositories.presets;

import obrauli.io.hamjoint.data.models.structures.Deletable;
import obrauli.io.hamjoint.data.models.structures.Named;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface NamedDeletableRepositories <Y extends Named & Deletable> {
    Optional<List<Y>> findByNameIgnoreCaseContainingAndActiveIsIn(String name, Set<Boolean> which);
    Page<Y> findByNameIgnoreCaseContainingAndActiveIsIn(String name, Pageable pageable, Set<Boolean> which);
}
