package obrauli.io.hamjoint.data.dto;

import obrauli.io.hamjoint.data.models.Ingredient;

import java.math.BigDecimal;

public class IngredientPrice {

    private Ingredient ingredient = null;
    private BigDecimal quantity = new BigDecimal(0);
    private BigDecimal startPrice = new BigDecimal(0);
    private BigDecimal endPrice = new BigDecimal(0);


    public IngredientPrice() {
    }

    public IngredientPrice(IngredientPrice i) {
        this.ingredient = i.ingredient;
        this.quantity = i.quantity;
        this.startPrice = i.startPrice;
        this.endPrice = i.endPrice;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public BigDecimal getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(BigDecimal startPrice) {
        this.startPrice = startPrice;
    }

    public BigDecimal getEndPrice() {
        return endPrice;
    }

    public void setEndPrice(BigDecimal endPrice) {
        this.endPrice = endPrice;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }
}
