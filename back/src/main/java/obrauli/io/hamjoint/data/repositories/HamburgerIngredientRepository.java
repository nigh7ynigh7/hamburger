package obrauli.io.hamjoint.data.repositories;

import obrauli.io.hamjoint.data.models.HamburgerIngredient;
import obrauli.io.hamjoint.data.models.structures.Identifiable;
import obrauli.io.hamjoint.data.reference.repository.RootRepository;

import java.util.Set;

public interface HamburgerIngredientRepository
        extends RootRepository<HamburgerIngredient, Long> {
    void deleteAllByHamburgerIdIn(Set<Long> id);
}
