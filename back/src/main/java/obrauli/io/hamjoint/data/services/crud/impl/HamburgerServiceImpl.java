package obrauli.io.hamjoint.data.services.crud.impl;

import obrauli.io.hamjoint.data.models.Hamburger;
import obrauli.io.hamjoint.data.models.HamburgerIngredient;
import obrauli.io.hamjoint.data.repositories.HamburgerRepository;
import obrauli.io.hamjoint.data.services.crud.HamburgerIngredientService;
import obrauli.io.hamjoint.data.services.crud.HamburgerService;
import obrauli.io.hamjoint.data.services.crud.IngredientService;
import obrauli.io.hamjoint.data.services.crud.common.impl.CommonAbstractServiceImpl;
import obrauli.io.hamjoint.utils.SelfAssertion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class HamburgerServiceImpl extends CommonAbstractServiceImpl<Long, Hamburger, HamburgerRepository> implements HamburgerService {

    @Autowired
    private HamburgerIngredientService hamburgerIngredientService;

    private final IngredientService ingredientService;

    public HamburgerServiceImpl(HamburgerRepository repository,
                                IngredientService ingredientService) {
        super(repository);
        this.ingredientService = ingredientService;
    }

    @Override
//    @Transactional(rollbackOn = { RuntimeException.class, Throwable.class })
    public Hamburger save(Hamburger incoming) {
        Set<HamburgerIngredient> ingredients = Optional.ofNullable(incoming)
                .map(Hamburger::getHamburgerIngredients).orElse(new HashSet<>());
        Hamburger saved = super.save(incoming);

        SelfAssertion.Assert(!ingredients.isEmpty(), "ingredients required");
        ingredients.stream().map(HamburgerIngredient::getId).forEach(this.hamburgerIngredientService::delete);
        saved.setHamburgerIngredients(
                ingredients
                        .stream()
                        .map(item -> {
                            HamburgerIngredient i = new HamburgerIngredient();
                            i.setHamburger(saved);
                            i.setIngredient(this.ingredientService.findById(item.getIngredient().getId()));
                            i.setQuantity(item.getQuantity());
                            return this.hamburgerIngredientService.save(i);
                        }).collect(Collectors.toSet()));

        return saved;
    }

    @Override
    public void validate(Hamburger toSave) {
        SelfAssertion.Assert(toSave.getName() != null && !toSave.getName().isBlank(), "name required");
    }

    @Override
    public void applyOnUpdate(Hamburger toSave, Hamburger newOrUpdating) {
        toSave.setName(newOrUpdating.getName());
        toSave.setHamburgerIngredients(null);
    }

    @Override
    public void postValidateUpdate(Hamburger toSave) {}

    @Override
    public void afterSaved(Hamburger saved) {
    }

}
