package obrauli.io.hamjoint.data.dto;

import obrauli.io.hamjoint.utils.SelfAssertion;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class DiscountStep {
    private BigDecimal startingValue = new BigDecimal(0);
    private BigDecimal endingValue = new BigDecimal(0);
    private BigDecimal discountApplied = new BigDecimal(0);
    private Set<IngredientPrice> ingredients = new HashSet<>();

    public DiscountStep() {
    }

    public DiscountStep(BigDecimal initial) {
        this.startingValue = startingValue;
        this.endingValue = startingValue;
    }

    public DiscountStep(BigDecimal initial, Set<IngredientPrice> ingredients) {
        this.startingValue = startingValue;
        this.endingValue = startingValue;
        if(ingredients != null) {
            this.ingredients = ingredients;
        }
    }

    public DiscountStep(DiscountStep d) {
        this.startingValue = d.startingValue;
        this.endingValue = d.endingValue;
        this.discountApplied = d.discountApplied;
        if(d.ingredients != null){
            this.ingredients = d.ingredients.stream().map(item -> new IngredientPrice(item)).collect(Collectors.toSet());
        }

    }

    public BigDecimal getStartingValue() {
        return startingValue;
    }

    public void setStartingValue(BigDecimal startingValue) {
        this.startingValue = startingValue;
    }

    public BigDecimal getEndingValue() {
        return endingValue;
    }

    public void setEndingValue(BigDecimal endingValue) {
        this.endingValue = endingValue;
    }

    public BigDecimal getDiscountApplied() {
        return discountApplied;
    }

    public void setDiscountApplied(BigDecimal discountApplied) {
        this.discountApplied = discountApplied;
    }

    public Set<IngredientPrice> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Set<IngredientPrice> ingredients) {
        this.ingredients = ingredients;
    }

    public BigDecimal getCalculatedTotal() {
        if(this.ingredients == null || this.ingredients.isEmpty()) return BigDecimal.ZERO;
        BigDecimal start = BigDecimal.ZERO;
        for (IngredientPrice ip: this.ingredients) {
            start = start.add(ip.getQuantity().multiply(ip.getEndPrice()));
        }
        return start;
    }
}
