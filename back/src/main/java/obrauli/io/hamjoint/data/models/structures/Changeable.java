package obrauli.io.hamjoint.data.models.structures;

import java.util.Date;

public interface Changeable {
    Date getCreatedAt();
    void setCreatedAt(Date date);

    Date getUpdatedAt();
    void setUpdatedAt(Date date);

}
