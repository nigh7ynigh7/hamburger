package obrauli.io.hamjoint.data.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import obrauli.io.hamjoint.data.models.names.Constraints;
import obrauli.io.hamjoint.data.models.names.Fields;
import obrauli.io.hamjoint.data.models.names.Sequences;
import obrauli.io.hamjoint.data.models.names.Tables;
import obrauli.io.hamjoint.data.models.structures.Identifiable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(
        name = Tables.INGREDIENT_PER_ORDER,
        uniqueConstraints =
        @UniqueConstraint(
                name = Constraints.INGREDIENT_ORDER_UNIQUE_CONSTRAINT,
                columnNames = { Fields.SIMPLE_ORDER_ID, Fields.INGREDIENT_ID }
        )
)
public class IngredientPerOrder implements Identifiable<Long>, Serializable {
    @Id
    @Column(name = Fields.ID)
    @SequenceGenerator(
            name = Sequences.INGREDIENT_PER_ORDER_SQ,
            sequenceName = Sequences.INGREDIENT_PER_ORDER_SQ,
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = Sequences.INGREDIENT_PER_ORDER_SQ
    )
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JoinColumn(name = Fields.SIMPLE_ORDER_ID, updatable = false, nullable = false)
    private SimpleClientRequest order;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = Fields.INGREDIENT_ID, updatable = false, nullable = false)
    private Ingredient ingredient;

    @Column(name = Fields.FRACTIONAL, updatable = false)
    private Boolean wasFractional = Ingredient.DEFAULT_FRACTIONAL;

    @Column(name = Fields.PRICING_AT_TIME, updatable = false, nullable = false)
    private BigDecimal pricingAtTime = new BigDecimal(0);

    @Column(name = Fields.FINAL_COST, updatable = false, nullable = false)
    private BigDecimal finalCost = new BigDecimal(0);

    @Column(name = Fields.DISCOUNT, updatable = false, nullable = false)
    private BigDecimal discount = new BigDecimal(0);

    @Column(name = Fields.QUANTITY, updatable = false, nullable = false)
    private BigDecimal quantityCount = new BigDecimal(0);

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public SimpleClientRequest getOrder() {
        return order;
    }

    public void setOrder(SimpleClientRequest order) {
        this.order = order;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public Boolean getWasFractional() {
        return wasFractional;
    }

    public void setWasFractional(Boolean wasFractional) {
        this.wasFractional = wasFractional;
    }

    public BigDecimal getPricingAtTime() {
        return pricingAtTime;
    }

    public void setPricingAtTime(BigDecimal pricingAtTime) {
        this.pricingAtTime = pricingAtTime;
    }

    public BigDecimal getFinalCost() {
        return finalCost;
    }

    public void setFinalCost(BigDecimal finalCost) {
        this.finalCost = finalCost;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getQuantityCount() {
        return quantityCount;
    }

    public void setQuantityCount(BigDecimal quantityCount) {
        this.quantityCount = quantityCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IngredientPerOrder that = (IngredientPerOrder) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
