package obrauli.io.hamjoint.data.models.structures;

import java.io.Serializable;

public interface Identifiable<T extends Serializable> {
    T getId();
    void setId(T id);
}
