package obrauli.io.hamjoint.data.reference.repository;

import obrauli.io.hamjoint.data.models.structures.Identifiable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

public interface RootRepository <Y extends Identifiable<Id>, Id extends Serializable> extends JpaRepository<Y, Id> {
    List<Y> findAllByIdIn(Set<Id> ids);
}
