package obrauli.io.hamjoint.data.services.crud.impl;

import obrauli.io.hamjoint.data.models.HamburgerIngredient;
import obrauli.io.hamjoint.data.repositories.HamburgerIngredientRepository;
import obrauli.io.hamjoint.data.services.crud.HamburgerIngredientService;
import obrauli.io.hamjoint.data.services.crud.HamburgerService;
import obrauli.io.hamjoint.data.services.crud.IngredientService;
import obrauli.io.hamjoint.data.services.crud.common.impl.CommonAbstractServiceImpl;
import obrauli.io.hamjoint.utils.SelfAssertion;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.util.Set;

@Service
public class HamburgerIngredientServiceImpl extends CommonAbstractServiceImpl<Long, HamburgerIngredient, HamburgerIngredientRepository> implements HamburgerIngredientService {

    @Autowired
    private HamburgerService hamburgerService;

    private final IngredientService ingredientService;

    public HamburgerIngredientServiceImpl(HamburgerIngredientRepository repository,
                                          IngredientService ingredientService) {
        super(repository);
        this.ingredientService = ingredientService;
    }

    @Override
    public void removeAllByHamburgerId(Set<Long> ids) {
        this.getRepository().deleteAllByHamburgerIdIn(ids);
    }

    @Override
    public void validate(HamburgerIngredient toSave) {
        SelfAssertion.Assert(toSave.getIngredient() != null && toSave.getIngredient().getId() != null, "no ingredient");
        SelfAssertion.Assert(toSave.getHamburger() != null && toSave.getHamburger().getId() != null, "no hamburger");
        SelfAssertion.Assert(toSave.getQuantity() != null, "no quantity");
        SelfAssertion.Assert(toSave.getQuantity().compareTo(BigDecimal.ZERO) >= 0, "cannot be zero");
    }

    @Override
    public void applyOnUpdate(HamburgerIngredient toSave, HamburgerIngredient newOrUpdating) {
        if(newOrUpdating.getIngredient() != null){
            toSave.setIngredient(this.ingredientService.findById(newOrUpdating.getIngredient().getId()));
        }
        if(newOrUpdating.getHamburger() != null){
            toSave.setHamburger(this.hamburgerService.findById(newOrUpdating.getHamburger().getId()));
        }
        toSave.setQuantity(newOrUpdating.getQuantity());
    }

    @Override
    public void postValidateUpdate(HamburgerIngredient toSave) {
        toSave.setIngredient(this.ingredientService.findById(toSave.getIngredient().getId()));
        toSave.setHamburger(this.hamburgerService.findById(toSave.getHamburger().getId()));
    }

    @Override
    public HamburgerIngredient delete(Long aLong) {
        if(ObjectUtils.isEmpty(aLong)) return null;
        HamburgerIngredient found = this.findById(aLong);
        this.getRepository().delete(found);
        return found;
    }

}
