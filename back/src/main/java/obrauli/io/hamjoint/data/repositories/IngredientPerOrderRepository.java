package obrauli.io.hamjoint.data.repositories;

import obrauli.io.hamjoint.data.models.IngredientPerOrder;
import obrauli.io.hamjoint.data.models.SimpleClientRequest;
import obrauli.io.hamjoint.data.reference.repository.RootRepository;

import java.util.List;

public interface IngredientPerOrderRepository
        extends RootRepository<IngredientPerOrder, Long> {
    List<IngredientPerOrder> findAllByOrder(SimpleClientRequest order);
}
