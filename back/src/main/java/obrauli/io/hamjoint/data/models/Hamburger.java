package obrauli.io.hamjoint.data.models;

import obrauli.io.hamjoint.data.models.names.Fields;
import obrauli.io.hamjoint.data.models.names.Sequences;
import obrauli.io.hamjoint.data.models.names.Tables;
import obrauli.io.hamjoint.data.models.structures.Changeable;
import obrauli.io.hamjoint.data.models.structures.Deletable;
import obrauli.io.hamjoint.data.models.structures.Identifiable;
import obrauli.io.hamjoint.data.models.structures.Named;
import org.springframework.util.ObjectUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = Tables.HAMBURGER)
public class Hamburger implements Identifiable<Long>, Named, Changeable, Deletable, Serializable {

    @Id
    @Column(name = Fields.ID)
    @SequenceGenerator(
            name = Sequences.HAMBURGER_ID_SQ,
            sequenceName = Sequences.HAMBURGER_ID_SQ,
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = Sequences.HAMBURGER_ID_SQ
    )
    private Long id;

    @Column(nullable = false, name = Fields.NAME)
    private String name;

    @Column(name = Fields.UPDATED_AT)
    private Date updatedAt = new Date();

    @Column(nullable = false, name = Fields.CREATED_AT, updatable = false)
    private Date createdAt = new Date();

    @Column(nullable = false, name = Fields.ACTIVE)
    private Boolean active = true;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "hamburger")
    private Set<HamburgerIngredient> hamburgerIngredients = new HashSet<>();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Date getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public Date getCreatedAt() {
        return createdAt;
    }

    @Override
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public Boolean getActive() {
        return active;
    }

    @Override
    public void setActive(Boolean active) {
        this.active = active;
    }

    public Set<HamburgerIngredient> getHamburgerIngredients() {
        return hamburgerIngredients;
    }

    public void setHamburgerIngredients(Set<HamburgerIngredient> hamburgerIngredients) {
        this.hamburgerIngredients = hamburgerIngredients;
    }

    @Transient
    public BigDecimal getValue(){
        BigDecimal val = BigDecimal.ZERO;
        if(ObjectUtils.isEmpty(this.getHamburgerIngredients())){
            return val;
        }
        for (HamburgerIngredient h : this.getHamburgerIngredients()) {
            if(h.getIngredient() != null && h.getIngredient().getValue() != null && h.getQuantity() != null) {
                val = val.add(h.getQuantity().multiply(h.getIngredient().getValue()));
            }
        }
        return val.setScale(2, RoundingMode.DOWN);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hamburger hamburger = (Hamburger) o;
        return Objects.equals(id, hamburger.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
