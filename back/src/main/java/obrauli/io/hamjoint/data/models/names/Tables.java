package obrauli.io.hamjoint.data.models.names;

public class Tables {
    public static final String INGREDIENTS = "INGREDIENTS";
    public static final String INGREDIENTS_HAMBURGER = "INGREDIENTS_HAMBURGER";
    public static final String HAMBURGER = "HAMBURGER";
    public static final String CLIENT_REQUEST = "REQUEST";
    public static final String APPLIED_DISCOUNTS = "APPLIED_DISCOUNTS";
    public static final String INGREDIENT_PER_ORDER = "INGREDIENT_PER_ORDER";
}
