package obrauli.io.hamjoint.data.services.crud.impl;

import obrauli.io.hamjoint.data.models.Ingredient;
import obrauli.io.hamjoint.data.repositories.IngredientRepository;
import obrauli.io.hamjoint.data.services.crud.IngredientService;
import obrauli.io.hamjoint.data.services.crud.common.impl.CommonAbstractServiceImpl;
import obrauli.io.hamjoint.utils.SelfAssertion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class IngredientServiceImpl extends CommonAbstractServiceImpl<Long, Ingredient, IngredientRepository> implements IngredientService {

    public IngredientServiceImpl(IngredientRepository repository) {
        super(repository);
    }

    @Override
    public void validate(Ingredient toSave) {
        SelfAssertion.Assert(toSave.getName() != null && !toSave.getName().isBlank(), "name is null");
        SelfAssertion.Assert(toSave.getValue() != null && toSave.getValue().compareTo(BigDecimal.ZERO) >= 0, "initial value cannot be null or zero");
    }

    @Override
    public void applyOnUpdate(Ingredient toSave, Ingredient newOrUpdating) {
        toSave.setName(newOrUpdating.getName());
        toSave.setValue(newOrUpdating.getValue());
    }

    @Override
    public void postValidateUpdate(Ingredient toSave) { }

}
