package obrauli.io.hamjoint.data.repositories.presets;

import obrauli.io.hamjoint.data.models.structures.Named;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface NamedRepositories <Y extends Named> {
    Optional<List<Y>> findByNameIgnoreCaseContaining(String name);
    Page<Y> findByNameIgnoreCaseContaining(String name, Pageable pageable);
}
