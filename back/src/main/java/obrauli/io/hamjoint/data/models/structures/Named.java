package obrauli.io.hamjoint.data.models.structures;

public interface Named {
    String getName();
    void setName(String name);
}
