package obrauli.io.hamjoint.data.models;


import com.fasterxml.jackson.annotation.JsonProperty;
import obrauli.io.hamjoint.data.models.names.Fields;
import obrauli.io.hamjoint.data.models.names.Sequences;
import obrauli.io.hamjoint.data.models.names.Tables;
import obrauli.io.hamjoint.data.models.structures.Changeable;
import obrauli.io.hamjoint.data.models.structures.Identifiable;
import obrauli.io.hamjoint.data.models.structures.Named;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;

@Entity
@Table(name = Tables.CLIENT_REQUEST)
public class SimpleClientRequest implements Identifiable<Long>, Named, Changeable, Serializable {

    @Id
    @Column(name = Fields.ID)
    @SequenceGenerator(
            name = Sequences.SIMPLE_CLIENT_ORDER_SQ,
            sequenceName = Sequences.SIMPLE_CLIENT_ORDER_SQ,
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = Sequences.SIMPLE_CLIENT_ORDER_SQ
    )
    private Long id;

    @Column(nullable = false, name = Fields.NAME)
    private String name;

    @Column(name = Fields.DESCRIPTION, length = 2048)
    private String description;

    @Column(nullable = false, name = Fields.VALUE)
    private BigDecimal total;

    @Column(nullable = false, name = Fields.FINAL_TOTAL)
    private BigDecimal finalTotal;

    @Column(name = Fields.UPDATED_AT)
    private Date updatedAt = new Date();

    @Column(nullable = false, name = Fields.CREATED_AT, updatable = false)
    private Date createdAt = new Date();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = Fields.HAMBURGER_ID, updatable = false)
    private Hamburger selectedHamburger;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "order")
    private List<IngredientPerOrder> ingredientPerOrders = new ArrayList<>();

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Column(nullable = false, name = Fields.UNIQUE_IDENTIFIER, updatable = false)
    private String uuid = UUID.randomUUID().toString();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getFinalTotal() {
        return finalTotal;
    }

    public void setFinalTotal(BigDecimal finalTotal) {
        this.finalTotal = finalTotal;
    }

    @Override
    public Date getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public Date getCreatedAt() {
        return createdAt;
    }

    @Override
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Hamburger getSelectedHamburger() {
        return selectedHamburger;
    }

    public void setSelectedHamburger(Hamburger selectedHamburger) {
        this.selectedHamburger = selectedHamburger;
    }

    public List<IngredientPerOrder> getIngredientPerOrders() {
        return ingredientPerOrders;
    }

    public void setIngredientPerOrders(List<IngredientPerOrder> ingredientPerOrders) {
        this.ingredientPerOrders = ingredientPerOrders;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleClientRequest that = (SimpleClientRequest) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
