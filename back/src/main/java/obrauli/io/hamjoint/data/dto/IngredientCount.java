package obrauli.io.hamjoint.data.dto;

import java.math.BigDecimal;

public class IngredientCount {
    private Long ingredientId;
    private BigDecimal quantity;

    public Long getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(Long ingredientId) {
        this.ingredientId = ingredientId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }
}
