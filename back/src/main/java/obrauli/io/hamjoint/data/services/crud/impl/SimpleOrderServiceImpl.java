package obrauli.io.hamjoint.data.services.crud.impl;

import obrauli.io.hamjoint.data.models.IngredientPerOrder;
import obrauli.io.hamjoint.data.models.SimpleClientRequest;
import obrauli.io.hamjoint.data.repositories.UserOrderRepository;
import obrauli.io.hamjoint.data.services.crud.IngredientPerOrderService;
import obrauli.io.hamjoint.data.services.crud.IngredientService;
import obrauli.io.hamjoint.data.services.crud.SimpleOrderService;
import obrauli.io.hamjoint.data.services.crud.common.impl.CommonAbstractServiceImpl;
import obrauli.io.hamjoint.exceptions.RegistryNotFoundException;
import obrauli.io.hamjoint.utils.SelfAssertion;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SimpleOrderServiceImpl extends CommonAbstractServiceImpl<Long, SimpleClientRequest, UserOrderRepository> implements SimpleOrderService {

    private final IngredientPerOrderService ingredientPerOrderService;
    private final IngredientService ingredientService;

    public SimpleOrderServiceImpl(UserOrderRepository repository,
                                  IngredientPerOrderService ingredientPerOrderService,
                                  IngredientService ingredientService) {
        super(repository);
        this.ingredientPerOrderService = ingredientPerOrderService;
        this.ingredientService = ingredientService;
    }

    @Override
    public void validate(SimpleClientRequest toSave) {
        SelfAssertion.Assert(toSave.getName() != null && !toSave.getName().isEmpty(), "name is blank");
        SelfAssertion.Assert(toSave.getTotal() != null, "total missing");
        SelfAssertion.Assert(toSave.getTotal().compareTo(BigDecimal.ZERO) >= 0, "total under zero");
        SelfAssertion.Assert(toSave.getFinalTotal() != null, "final total missing");
        SelfAssertion.Assert(toSave.getFinalTotal().compareTo(BigDecimal.ZERO) >= 0, "final total under zero");
    }

    @Override
    public void applyOnUpdate(SimpleClientRequest toSave, SimpleClientRequest newOrUpdating) {}

    @Override
    public void postValidateUpdate(SimpleClientRequest toSave) {}

    @Override
    @Transactional
    public SimpleClientRequest save(SimpleClientRequest incoming) {
        List<IngredientPerOrder> ingredients = incoming.getIngredientPerOrders();

        incoming.setIngredientPerOrders(null);
        SimpleClientRequest output = super.save(incoming);

        output.setIngredientPerOrders(
                ingredients.stream()
                        .map(item -> {
                            item.setOrder(output);
                            item.setIngredient(this.ingredientService.findById(item.getIngredient().getId()));
                            return ingredientPerOrderService.save(item);
                        })
                        .collect(Collectors.toList()));

        return output;
    }

    @Override
    public SimpleClientRequest findByUuid(String id) {
        return this.getRepository().findByUuid(id).orElseThrow(() -> new RegistryNotFoundException("no order with the uuid found"));
    }
}
