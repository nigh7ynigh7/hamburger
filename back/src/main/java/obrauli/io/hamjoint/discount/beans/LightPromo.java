package obrauli.io.hamjoint.discount.beans;

import obrauli.io.hamjoint.data.dto.CompactUserRequest;
import obrauli.io.hamjoint.data.dto.DiscountStep;
import obrauli.io.hamjoint.data.dto.IngredientCount;
import obrauli.io.hamjoint.data.dto.IngredientPrice;
import obrauli.io.hamjoint.data.models.Hamburger;
import obrauli.io.hamjoint.data.models.Ingredient;
import obrauli.io.hamjoint.data.services.crud.IngredientService;
import obrauli.io.hamjoint.discount.DiscountProcessors;
import obrauli.io.hamjoint.enums.DiscountTypes;
import obrauli.io.hamjoint.enums.IngredientType;
import obrauli.io.hamjoint.utils.BigDecimalUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Component
public class LightPromo implements DiscountProcessors {

    private final String PROMO = "0.1";

    @Override
    public DiscountTypes getType() {
        return DiscountTypes.LIGHT;
    }

    @Override
    public int getWeight() {
        return 0;
    }

    @Override
    public DiscountStep applyDiscount(List<DiscountStep> previousSteps) {


        if(previousSteps == null || previousSteps.isEmpty()) return null;
        DiscountStep last = previousSteps.get(previousSteps.size() - 1);

        AtomicReference<Boolean> hasBacon = new AtomicReference<>(false);
        AtomicReference<Boolean> hasLeaf = new AtomicReference<>(false);

        last.getIngredients().stream().map(IngredientPrice::getIngredient).forEach(item -> {
            if(IngredientType.BACON.equals(item.getIngredientType())) {
                hasBacon.set(true);
            }
            else if(IngredientType.LEAVES.equals(item.getIngredientType())) {
                hasLeaf.set(true);
            }
        });

        if(!hasBacon.get() && hasLeaf.get()) {
            last.getIngredients().forEach(item -> {
                BigDecimal value = item.getEndPrice();
                item.setEndPrice(BigDecimalUtils.apply10PercentDiscount(value));
            });
            last.setEndingValue(last.getCalculatedTotal());
            last.setDiscountApplied(BigDecimalUtils.calculateDiscount(last.getStartingValue(), last.getEndingValue()));
            return last;
        }

        return null;
    }
}
