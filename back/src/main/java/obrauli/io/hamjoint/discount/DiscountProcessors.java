package obrauli.io.hamjoint.discount;

import obrauli.io.hamjoint.data.dto.DiscountStep;
import obrauli.io.hamjoint.enums.DiscountTypes;

import java.util.List;

public interface DiscountProcessors {
    DiscountTypes getType();
    int getWeight();
    DiscountStep applyDiscount(List<DiscountStep> previousSteps);
}
