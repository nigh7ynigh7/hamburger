package obrauli.io.hamjoint.discount.beans;

import obrauli.io.hamjoint.data.dto.DiscountStep;
import obrauli.io.hamjoint.data.dto.IngredientPrice;
import obrauli.io.hamjoint.discount.DiscountProcessors;
import obrauli.io.hamjoint.enums.DiscountTypes;
import obrauli.io.hamjoint.utils.BigDecimalUtils;
import obrauli.io.hamjoint.utils.IngredientUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Component
public class SoMuchBacon implements DiscountProcessors {

    @Override
    public DiscountTypes getType() {
        return DiscountTypes.SO_MUCH_MEAT;
    }

    @Override
    public int getWeight() {
        return 10;
    }

    @Override
    public DiscountStep applyDiscount(List<DiscountStep> previousSteps) {

        if(previousSteps == null || previousSteps.isEmpty()) return null;

        DiscountStep last = new DiscountStep(previousSteps.get(previousSteps.size() - 1));

        AtomicReference<Boolean> save = new AtomicReference<>(false);

        last.getIngredients().forEach(item -> {
            if(IngredientUtils.isBacon(item.getIngredient())
                    && BigDecimalUtils.hasThreeOrMore(item.getQuantity())){
                save.set(true);
                item.setEndPrice(BigDecimalUtils.applyDiscountThreeForTwo(item.getQuantity(), item.getEndPrice()));
            }
        });


        if(save.get()) {
            last.setEndingValue(last.getCalculatedTotal());
            last.setDiscountApplied(BigDecimalUtils.calculateDiscount(last.getStartingValue(), last.getEndingValue()));
            return last;
        }

        return null;
    }
}
