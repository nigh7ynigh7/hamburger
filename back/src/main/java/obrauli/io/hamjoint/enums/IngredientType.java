package obrauli.io.hamjoint.enums;

public enum IngredientType {
    LEAVES,
    VEGGIES,
    BREAD,
    CONDIMENTS,
    DAIRY_CHEESE,
    MEAT,
    HEAVY_MEAT,
    CHICKEN_MEAT,
    CHICKEN_BASED,
    VEGGIE_MEAT,
    SOY_MEAT,
    SEA_FOOT,
    EXOTIC,
    BACON;
}
