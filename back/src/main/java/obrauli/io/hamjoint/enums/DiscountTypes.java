package obrauli.io.hamjoint.enums;

public enum DiscountTypes {
    LIGHT,
    SO_MUCH_MEAT,
    SO_MUCH_CHEESE;
}
