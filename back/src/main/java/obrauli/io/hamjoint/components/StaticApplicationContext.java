package obrauli.io.hamjoint.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.stereotype.Component;

import javax.swing.text.html.Option;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class StaticApplicationContext {



    private static GenericApplicationContext genericApplicationContext = null;

    @Autowired
    public StaticApplicationContext(GenericApplicationContext applicationContext) {
        genericApplicationContext = applicationContext;
    }

    public static Optional<GenericApplicationContext> getCtx() {
        return Optional.ofNullable(genericApplicationContext);
    }

    /**
     * gets a collection of beans of a specific type/implementation: can be used in a static context.
     * regarding said static context: BEWARE!
     * @param clzz class of expected type
     * @param <T> expected type
     * @return an optional of list
     */
    public static <T> Optional<List<T>> getBeans(Class<T> clzz) {
        if(clzz == null) {
            return Optional.empty();
        }

        var beans = genericApplicationContext.getBeanNamesForType(clzz);
        if(beans == null || beans.length == 0) {
            return Optional.empty();
        }


        return Optional.of(
                Arrays.stream(beans)
                        .map(item -> genericApplicationContext.getBean(item, clzz))
                        .collect(Collectors.toList())
        );
    }
}
