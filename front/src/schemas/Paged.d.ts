interface PagedContent<T> {
  content : T[];
  last : boolean;
  totalPages : number;
  totalElements : number;
}