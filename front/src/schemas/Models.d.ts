interface Identifiable<T> {
  id : T;
}

interface Named {
  name : string;
}

interface Changeable {
  updatedAt : string;
  createdAt : string;
}

interface Deletable {
  active : boolean;
}

type IngredientType =  'LEAVES' | 'VEGGIES'
  | 'BREAD' | 'CONDIMENTS' | 'DAIRY_CHEESE'
  | 'MEAT' | 'HEAVY_MEAT' | 'CHICKEN_MEAT'
  | 'VEGGIE_MEAT' | 'SOY_MEAT' | 'BACON';

interface Ingredient extends Identifiable<number>, Named, Changeable, Deletable {
  allowFractional : boolean;
  ingredientType : IngredientType;
  hamburgers : HamburgerIngredient[];
  value : number;
}

interface Hamburger extends Identifiable<number>, Named, Changeable, Deletable {
  hamburgerIngredients : HamburgerIngredient[];
}

interface HamburgerIngredient extends Identifiable<number> {
  quantity : number;
  ingredient : Ingredient;
  hamburger : Hamburger;
}

interface Order extends Identifiable<number>, Named, Changeable {
  description : string;
  total : number;
  finalTotal : number;
  selectedHamburger : Hamburger;
  ingredientPerOrders : IngredientPerOrder[];
  uuid ?: string;
}

interface IngredientPerOrder extends Identifiable<number>{
  order : Order;
  ingredient : Ingredient;
  wasFractional : boolean;
  pricingAtTime : number;
  finalCost : number;
  discount : number;
  quantityCount : number;
}