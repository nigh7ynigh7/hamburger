
interface IngredientCount {
  ingredientId ?: number;
  quantity : number;
}

interface CompactUserRequest {
  hamburgerId : number;
  overwriteIngredients : IngredientCount[];
  forWhom : string;
  description : string;
}