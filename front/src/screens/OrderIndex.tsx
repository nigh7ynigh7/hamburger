import { Heading } from "@chakra-ui/react";
import React from "react";
import { Route, Switch } from "react-router-dom";
import OrderingLayout from "../layouts/OrderingLayout";
import * as coreRoutes from '../constants/RouterConstants';
import UserOrderIndex from "./ordering/UserOrderIndex";
import OrderSummary from "./ordering/OrderSummary";
import AdminIndex from "./admin/AdminIndex";

export const OrderIndex : React.FC<any> = (props) => {
  return (
    <OrderingLayout>
      <Switch>
        <Route strict={true} path={coreRoutes.YOUR_ORDER} component={() => {
          return (
            <React.Suspense fallback={<div/>}>
              <OrderSummary />
            </React.Suspense>
          )
        }} />
        <Route strict={true} path={coreRoutes.ROOT} component={() => {
          return (
            <React.Suspense fallback={<div/>}>
              <UserOrderIndex />
            </React.Suspense>
          )
        }} />
      </Switch>
    </OrderingLayout>
  )
}

export default OrderIndex;