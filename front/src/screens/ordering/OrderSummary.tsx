import { Center, Spinner, Table, Tbody, Td, Tr, useToast, VStack, Text, Thead, Th, StackDivider, Button, Box, Heading } from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { getPlacedOrder } from "../../api/Orders";
import * as coreRoutes from '../../constants/RouterConstants';

interface Props {
  order ?: Order;
}

export const OrderSummary : React.FC<Props> = (props) => {
  const incomingOrder = props.order;
  const { id } = useParams<{id : string}>();
  const toast = useToast();
  const [ loading, setLoading ] = useState<boolean>(true);
  const [ order, setOrder ] = useState<Order>();
  useEffect(() => {
    if(incomingOrder){
      setOrder(incomingOrder);
      return setLoading(false);
    }
    setLoading(true);
    getPlacedOrder(id)
      .then(item => setOrder(item))
      .catch(err => {
        toast({
          title : 'could not fetch your order',
          isClosable : true,
          position : "top-left"
        })
      })
      .finally(() => setLoading(false))
  }, [id, incomingOrder]);

  if(loading || !order) {
    return (
      <Center>
        <Spinner
          thickness="4px"
          speed="0.65s"
          emptyColor="gray.200"
          color="blue.500"
          size="xl"
        />
      </Center>
    )
  }

  return (
    <VStack divider={<StackDivider borderColor="gray.200" />}>
      {
        !incomingOrder ? (
          <Heading as='h3'>Your order</Heading>
        ) : undefined
      }
      <Table variant='unstyled' size='sm'>
        <Tbody>
          <Tr>
            <Td fontWeight='bold'>For whom?</Td>
            <Td textAlign='right'>{order.name}</Td>
          </Tr>
          {
            order.description ? (
              <>
                <Tr>
                  <Td fontWeight='bold' colSpan={2}>Description:</Td>
                </Tr>
                <Tr>
                  <Td colSpan={2}>
                    <Text as='em'>{order.description}</Text>
                  </Td>
                </Tr>
              </>
            ) : undefined
          }
          <Tr>
            <Td fontWeight='bold'>Sub Total:</Td>
            <Td textAlign='right'>{order.total}</Td>
          </Tr>
          <Tr>
            <Td fontWeight='bold'>Total:</Td>
            <Td textAlign='right'>{order.finalTotal}</Td>
          </Tr>
        </Tbody>
      </Table>
      <Table size='sm'>
        <Thead>
          <Tr>
            <Th>Ingredient</Th>
            <Th isNumeric>Quantity</Th>
            <Th isNumeric>Value</Th>
            <Th isNumeric>Item Total</Th>
          </Tr>
        </Thead>
        {
          (order.ingredientPerOrders || []).map(item => {
            return (
              <Tr key={item.id}>
                <Td>{item.ingredient?.name}</Td>
                <Td isNumeric>{item.quantityCount}</Td>
                <Td isNumeric>{item.pricingAtTime}</Td>
                <Td isNumeric>{item.finalCost}</Td>
              </Tr>
            )
          })
        }
      </Table>
      {
        !incomingOrder ? (
          <Box>
            <Link to={coreRoutes.ORDERS}>
              <Button>
                order another
              </Button>
            </Link>
          </Box>
        ) : undefined
      }
    </VStack>
  )
}

export default OrderSummary;