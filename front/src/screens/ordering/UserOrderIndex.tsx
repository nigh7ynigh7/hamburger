import { AddIcon, DeleteIcon } from "@chakra-ui/icons";
import { Badge, Box, Button, Center, Heading, IconButton, Input, NumberDecrementStepper, NumberIncrementStepper, NumberInput, NumberInputField, NumberInputStepper, Popover, PopoverArrow, PopoverBody, PopoverCloseButton, PopoverContent, PopoverHeader, PopoverTrigger, Stack, Table, Tbody, Td, Text, Textarea, Th, Thead, toast, Tooltip, Tr, useToast, VStack } from "@chakra-ui/react";
import { CUIAutoComplete } from "chakra-ui-autocomplete";
import React, { useEffect, useState } from "react";
import { HamburgerCrud, HAMBURGER_API, INGREDIENT_API, ORDER_API } from "../../api/CrudCollection";
import { sendOrder } from "../../api/Orders";
import VerticalSpacing from "../../components/VerticalSpacing";
import * as coreRoutes from '../../constants/RouterConstants';
import { Link } from 'react-router-dom';

const toVal = (h : Identifiable<number> & Named) => ({label : h.name, value : String(h.id)});

interface IngredientCount {
  ingredient ?: Ingredient;
  quantity : number;
}

const calculateStep = (order : Partial<CompactUserRequest>, last : number) => {
  let count = 0;
  if(!order.hamburgerId) return count;
  count ++;
  if(!order.overwriteIngredients 
      || last == 0 
      || order.overwriteIngredients && order.overwriteIngredients.some(i => !i.ingredientId || i.quantity === undefined)) {
    return count;
  }
  count ++;
  if(!order.forWhom || last == 1) {
    return count;
  }
  return count + 1;
}

const calculateError = (order : Partial<CompactUserRequest>, step : number) => {
  if(!order.hamburgerId) return 'NO_ID';
  if(step >= 1) {
    if(order.overwriteIngredients && order.overwriteIngredients.some(i => i.quantity == undefined || i.quantity < 0 && !i.ingredientId )) {
      return 'INVALID_INGREDIENT';
    }
  }
  if(step >= 2) {
    if(!order.forWhom) {
      return 'NO_IDENTIFICATION';
    }
  }
}


export const UserOrderIndex : React.FC<any> = (props) =>  {
  const [ step, setStep ] = useState<number>(0);
  const [ error, setError ] = useState<string>();
  const [ order, setOrder ] = useState<Partial<CompactUserRequest>>({});
  const toast = useToast();

  const [hamburgers, setHamburgers] = useState<Hamburger[]>([]);
  const [selectedHamburgers, setSelectedHamburgers] = useState<Hamburger[]>([]);
  const [ingredients, setIngredients] = useState<IngredientCount[]>([]);

  const [hamburgerQuery, setHamburgerQuery] = useState<string>();
  useEffect(() => {
    HAMBURGER_API.getPaged(0, hamburgerQuery).then(items => setHamburgers(items.content || []));
  }, [hamburgerQuery]);

  useEffect(() => {
    setOrder(input => {
      return {
        ...input,
        hamburgerId : selectedHamburgers[0]?.id,
        overwriteIngredients : ingredients.map(item => ({
          ingredientId : item.ingredient?.id,
          quantity : item.quantity
        }))
      }
    })
  }, [selectedHamburgers, ingredients])

  useEffect(() => {
    setError(calculateError(order, step))
  }, [order, step]);

  const [ingredientsApi, setIngredientsApi] = useState<Ingredient[]>([]);
  const [ingredientQuery, setIngredientQuery] = useState<string>('');

  useEffect(() => {
    INGREDIENT_API.getPaged(0, ingredientQuery).then(items => setIngredientsApi(items.content || []));
  }, [ingredientQuery])

  const [ sending, setSending ] = useState<boolean>(false);
  const [ uuid, setUuid ] = useState<string>();

  const sendOrderToServer = async () => {
    setSending(true);
    try {
      setUuid(await sendOrder(order as CompactUserRequest))
    }
    catch(err) {
      console.log(err);
      toast({
        title : 'failed to send hamburger',
        position : 'top-right',
        isClosable : true
      })
    }
    finally {
      setSending(false);
    }
  }

  if(uuid) {
    return (
      <VStack>
        <Heading as='h4'>We're done here!</Heading>
        <Link to={coreRoutes.YOUR_ORDER.replace(':id', uuid)}>
          <Button>click here...</Button>
        </Link>
        <Text>... to see your order</Text>
      </VStack>
    );
  }

  return (
    <div>
        <VStack>
          <Center>
            <Heading>Order your sandwich now!</Heading>
          </Center>

          <Box width="100%" boxShadow="base" borderWidth='1px' borderRadius='2px' p='10px'>
            <Heading as='h4'>Pick your poison</Heading>
            <CUIAutoComplete
              label='select a hamburger'
              labelStyleProps={{display : 'none'}}
              placeholder={'search for an option'}
              hideToggleButton={true}
              disableCreateItem={true}
              onSelectedItemsChange={item => {
                if(item.selectedItems && item.selectedItems.length) {
                  const last = item.selectedItems[item.selectedItems.length - 1];
                  setSelectedHamburgers(hamburgers.filter(i => String(i.id) == last.value))
                }
                else {
                  setSelectedHamburgers([]);
                }
              }}
              optionFilterFunc={(_, query) => {
                setHamburgerQuery(query || '');
                return hamburgers.map(toVal);
              }}
              items={hamburgers.map(toVal)}
              selectedItems={selectedHamburgers.map(toVal)}
            />
            <VerticalSpacing size={10} />
            <Stack direction='row'>
              {
                selectedHamburgers && selectedHamburgers.length ?
                  (selectedHamburgers[0].hamburgerIngredients || [])
                    .map(item => <Badge colorScheme='yellow' key={item.id}>{item?.quantity || 0}x {item?.ingredient?.name}; ${item?.ingredient?.value} ea.</Badge>)
                  : ''
              }
            </Stack>
          </Box>

          {
            step >= 1 ? (
              <>
                <Box width="100%" boxShadow="base" borderWidth='1px' borderRadius='2px' p='10px'>
                  <Heading as='h4'>Modify your hamburger</Heading>
                  <Table>
                    <Thead>
                      <Tr>
                        <Th>Ingredient</Th>
                        <Th>Price</Th>
                        <Th>Quantity</Th>
                        <Th>Actions</Th>
                      </Tr>
                    </Thead>
                    <Tbody>
                      {
                        ingredients.map((item, i) => {
                          return (
                            <Tr key={`${item.ingredient?.id}_${i}`}>
                              <Td>
                                <Popover>
                                  <PopoverTrigger>
                                    <Button>
                                      <Tooltip label="click to change ingredient" aria-label="click to change ingredient" placement="top">
                                          {
                                            !item.ingredient || !item.ingredient.id ? (
                                              'no ingredient'
                                            ) : item.ingredient?.name
                                          }
                                      </Tooltip>
                                    </Button>
                                  </PopoverTrigger>
                                  <PopoverContent>
                                    <PopoverArrow />
                                    <PopoverCloseButton />
                                    <PopoverHeader><Text fontWeight="bold">Pick an ingredient</Text></PopoverHeader>
                                    <PopoverBody>
                                      <CUIAutoComplete
                                        label={'select your ingredient'}
                                        placeholder={'search for an ingredient'}
                                        hideToggleButton={true}
                                        disableCreateItem={true}
                                        labelStyleProps={{display : 'none'}}
                                        onSelectedItemsChange={item => {
                                          if(item.selectedItems && item.selectedItems.length) {
                                            const last = item.selectedItems[item.selectedItems.length - 1];
                                            setIngredients(old => {
                                              old[i].ingredient = ingredientsApi.filter(i => String(i.id) == last.value)[0];
                                              return [...old];
                                            })
                                          }
                                          else {
                                            setIngredients(old => {
                                              old[i].ingredient = undefined;
                                              return [...old];
                                            })
                                          }
                                        }}
                                        optionFilterFunc={(_, query) => {
                                          setIngredientQuery(query || '');
                                          return ingredientsApi.map(toVal);
                                        }}
                                        items={ingredientsApi.filter(item => !ingredients.some(inner => item.id === inner.ingredient?.id)).map(toVal)}
                                        selectedItems={item.ingredient ? [toVal(item.ingredient)] : [] }
                                      />
                                    </PopoverBody>
                                  </PopoverContent>
                                </Popover>
                              </Td>
                              <Td isNumeric>{item.ingredient?.value}</Td>
                              <Td>
                                <NumberInput value={item?.quantity || 0.0}
                                    step={item?.ingredient?.allowFractional ? 0.01 : 1 }
                                    onChange={val => {
                                      let value = Number(val) || 0;
                                      setIngredients(old => {
                                        old[i].quantity = item?.ingredient?.allowFractional ? value : Math.floor(value);
                                        return [...old]
                                      })
                                    }}>
                                  <NumberInputField />
                                  <NumberInputStepper>
                                    <NumberIncrementStepper />
                                    <NumberDecrementStepper />
                                  </NumberInputStepper>
                                </NumberInput>
                              </Td>
                              <Td>
                                <IconButton
                                    aria-label='delete'
                                    icon={<DeleteIcon />}
                                    colorScheme='red'
                                    onClick={() => {
                                      setIngredients(old => {
                                        old.splice(i, 1);
                                        return [...old];
                                      })
                                    }} 
                                />
                              </Td>
                            </Tr>
                          )
                        })
                      }
                      <Tr>
                        <Td colSpan={3}/>
                        <Td>
                          <IconButton
                              aria-label='add'
                              icon={<AddIcon />}
                              colorScheme='blue'
                              onClick={() => {
                                if(!ingredients.some(i => !i.ingredient || !i.ingredient.id)) {
                                  setIngredients(old => {
                                    return [...old, {ingredient : undefined, quantity : 0}]
                                  })
                                }
                              }} 
                          />
                        </Td>
                      </Tr>
                    </Tbody>
                  </Table>
                </Box>
              </>
            ) : undefined
          }

          {
            step >= 2 ? (
              <Box width="100%" boxShadow="base" borderWidth='1px' borderRadius='2px' p='10px'>
                <Heading as='h4'>Identification</Heading>
                <VStack>
                  <Box w='100%'>
                    <Text fontWeight='bold'>
                      How can i identify you?{' '}
                      {
                        order.forWhom ? (
                          <>
                            ({255 - order.forWhom.length} left)
                          </>
                        ) : undefined
                      }
                    </Text>
                    <Input
                        value={order.forWhom}
                        placeholder='John Wick'
                        maxLength={255}
                        onChange={val => setOrder(ord => ({...ord, forWhom : val.target.value || ''}))} 
                    />
                  </Box>
                  <Box w='100%'>
                    <Text fontWeight='bold'>
                      Anything else?{' '}
                      {
                        order.description ? (
                          <>
                            ({2048 - order.description.length} left)
                          </>
                        ) : undefined
                      }
                    </Text>
                    <Textarea 
                      placeholder="Meat cooked medium rare, your address?"
                      maxLength={2048}
                      value={order.description}
                      onChange={val => setOrder(ord => ({...ord, description : val.target.value || ''}))} 
                    />
                  </Box>
                </VStack>
              </Box>
            ) : undefined
          }
          
          <Button
              isActive={!error}
              isLoading={sending}
              onClick={() => {
                if(step == 2) {
                  return sendOrderToServer();
                }
                let current = step;
                let next = step;
                setStep(next = calculateStep(order, step));
                if(current == next) return;
                if(next == 1) {
                  setIngredients(selectedHamburgers[0]?.hamburgerIngredients?.map(item => ({
                    ingredient : item.ingredient,
                    quantity : item.quantity
                  })) || []);
                }
              }}>
            {
              step >= 2 ? 'order' : 'next'
            }
          </Button>

        </VStack>
    </div>
  )
}

export default UserOrderIndex;