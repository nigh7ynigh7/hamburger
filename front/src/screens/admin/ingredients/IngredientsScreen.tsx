import { DeleteIcon, EditIcon, ViewIcon } from "@chakra-ui/icons";
import { Box, Button, Center, Heading, Input, Tooltip, VStack, Text, HStack, Table, Thead, Tr, Th, useToast, Tbody, Spinner, Td, IconButton, useDisclosure, BreadcrumbItem, BreadcrumbLink, Breadcrumb } from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import { Link, Route, Switch } from "react-router-dom";
import { INGREDIENT_API } from "../../../api/CrudCollection";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
} from "@chakra-ui/react"
import {
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverHeader,
  PopoverBody,
  PopoverFooter,
  PopoverArrow,
  PopoverCloseButton,
} from "@chakra-ui/react"
import IngredientsDetailsModal from "./IngredientsDetailsModal";
import IngredientsCreationModal from "./IngredientsCreationModal";
import { IngredientTypeApiInstance } from "../../../api/EnumCollections";
import * as coreAdminRoutes from '../../../constants/RouterAdminConstants';


export const IngredientsScreen : React.FC<any> = (props) => {
  const toast = useToast();
  const [ page, setPage ] = useState(0);
  const [ query, setQuery ] = useState('');
  const [ times, setTime ] = useState(0);
  const [ saving, setSaving ] = useState(false);
  const [ pagedResult, setPagedResult ] = useState<PagedContent<Ingredient>>();
  const [ activeIngredient, setActiveIngredient ] = useState<Partial<Ingredient>>();
  const detailsModal = useDisclosure();
  const modifyModal = useDisclosure();

  useEffect(() => {
    INGREDIENT_API.getPaged(page, query)
      .then(item => setPagedResult(item))
      .catch(err => {
        toast({
          title : 'failed to load page',
          position : 'top-right',
          isClosable : true,
        });
      })
  }, [page, query, times])


  
  if(!pagedResult) {
    return (
      <Center>
        <Spinner
          thickness="4px"
          speed="0.65s"
          emptyColor="gray.200"
          color="blue.500"
          size="xl"
        />
      </Center>
    )
  }

  const addTimes = () => setTime(i => i + 1);

  const content = pagedResult.content;

  return (
    <>
      <VStack>
        <Heading as='h3' w='100%'>Ingredients</Heading>
        <Breadcrumb w='100%' p='10px' borderWidth='1px' borderRadius='3px'>
          <BreadcrumbItem>
            <BreadcrumbLink>
              <Link to={coreAdminRoutes.ADMIN_ROOT}>
                Admin
              </Link>
            </BreadcrumbLink>
          </BreadcrumbItem>
          <BreadcrumbItem>
            <BreadcrumbLink>
              <Link to={coreAdminRoutes.INGREDIENTS}>
                Ingredients
              </Link>
            </BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumb>
        <HStack w='100%'>
          <Input
            placeholder='search'
            value={query}
            onChange={e => setQuery(e.currentTarget.value)}
          />
          <Button onClick={() => {

            setActiveIngredient({});
            modifyModal.onOpen();
          }}>new</Button>
        </HStack>
        <Table>
          <Thead>
            <Tr>
              <Th>Ingredient</Th>
              <Th isNumeric>Value</Th>
              <Th>Actions</Th>
            </Tr>
          </Thead>

          <Tbody>
            
            {
              content.map((item, i) => {
                return (
                  <Tr>
                    <Td>{item.name}</Td>
                    <Td isNumeric>{item.value}</Td>
                    <Td>
                      <IconButton 
                          size='sm'
                          icon={<EditIcon />}
                          aria-label='view'
                          onClick={(e) => {
                            setActiveIngredient(item);
                            modifyModal.onOpen();
                          }}
                      />
                      {' '}
                      <IconButton 
                          size='sm'
                          icon={<ViewIcon />}
                          aria-label='view'
                          onClick={(e) => {
                            setActiveIngredient(item);
                            detailsModal.onOpen();
                          }}
                      />
                      {' '}
                      <Popover>
                        <PopoverTrigger>
                          <IconButton 
                              size='sm'
                              icon={<DeleteIcon />}
                              aria-label='delete'
                          />
                        </PopoverTrigger>
                        <PopoverContent>
                          <PopoverArrow />
                          <PopoverCloseButton />
                          <PopoverHeader>are you sure?</PopoverHeader>
                          <PopoverBody>
                            <p>this action cannot be undone</p>
                            <Button 
                              onClick={() => {
                                INGREDIENT_API.delete(item.id)
                                  .then(() => addTimes())
                                  .catch(err => {
                                    toast({
                                      title : 'failed to delete ingredient',
                                      position : 'top-right',
                                      isClosable : true,
                                    });
                                  });
                              }}
                            >
                              delete
                            </Button>
                          </PopoverBody>
                        </PopoverContent>
                      </Popover>
                    </Td>
                  </Tr>
                )
              })
            }          
          </Tbody>
        </Table>
      </VStack>

      <Modal isOpen={detailsModal.isOpen} onClose={detailsModal.onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Details: ingredient</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <IngredientsDetailsModal ingredient={activeIngredient as Ingredient} />
          </ModalBody>
          <ModalFooter>
            <Button variant="ghost" onClick={detailsModal.onClose}>ok</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
      
      <Modal isOpen={modifyModal.isOpen} onClose={modifyModal.onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Modal Title</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            {
              activeIngredient ? (
                <IngredientsCreationModal ingredient={activeIngredient} onChange={(i) => {
                  setActiveIngredient(i);
                }}/>
              ) : undefined
            }
          </ModalBody>
          <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={() => {
              setSaving(true);
              if(activeIngredient){
                INGREDIENT_API.save(activeIngredient)
                  .then(() => {
                    setActiveIngredient(undefined);
                    modifyModal.onClose();
                    addTimes();
                  })
                  .catch(err => {
                    toast({
                      title : 'could not save ingredient',
                      position : 'top-right',
                      isClosable : true,
                    });
                  })
                  .finally(() => setSaving(false));
                }
            }}>
              save
            </Button>
            <Button variant="ghost" onClick={() => {
              setActiveIngredient(undefined);
              modifyModal.onClose();
            }}>cancel</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  )
}

export default IngredientsScreen;