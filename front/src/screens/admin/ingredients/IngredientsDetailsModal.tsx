import { Box, Button, Center, Heading, Input, Tooltip, VStack, Text, useDisclosure, Table, Tbody, Tr, Td } from "@chakra-ui/react";
import React, { useState } from "react";
import { Route, Switch } from "react-router-dom";

interface Props {
  ingredient ?: Ingredient;
}

export const IngredientsDetailsModal : React.FC<Props> = (props) => {
  if(!props.ingredient) {
    return <Text>nothing selected</Text>
  }
  return (
    <Table variant='unstyled' size='sm'>
      <Tbody>
        <Tr>
          <Td>Id</Td>
          <Td>{props.ingredient.id}</Td>
        </Tr>
        <Tr>
          <Td>Name</Td>
          <Td>{props.ingredient.name}</Td>
        </Tr>
        <Tr>
          <Td>Value</Td>
          <Td>{props.ingredient.value}</Td>
        </Tr>
        <Tr>
          <Td>Type</Td>
          <Td>{props.ingredient.ingredientType}</Td>
        </Tr>
        <Tr>
          <Td>Fractional</Td>
          <Td>{props.ingredient.allowFractional ? 'yes' : 'no' }</Td>
        </Tr>
      </Tbody>
    </Table>
  )
}

export default IngredientsDetailsModal;