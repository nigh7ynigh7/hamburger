
import { Box, Button, Center, Heading, Input, Tooltip, VStack, Text, useDisclosure, Table, Tbody, Tr, Td, FormControl, FormLabel, FormHelperText, Select } from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import { Route } from "react-router-dom";
import {
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
} from "@chakra-ui/react"
import { Switch } from "@chakra-ui/react"
import { IngredientTypeApiInstance } from "../../../api/EnumCollections";

interface Props {
  ingredient : Partial<Ingredient>;
  onChange : (ingredient : Ingredient) => void;
}

export const IngredientsCreationModal : React.FC<Props> = (props) => {
  const [ ingredientTypes, setIngredientTypes ] = useState<string[]>([]);
  
  useEffect(() => {
    IngredientTypeApiInstance.getEnum().then(setIngredientTypes);
  }, [])

  if(props.ingredient && !props.ingredient.ingredientType && ingredientTypes.length) {
    props.onChange({
      ...props.ingredient,
      ingredientType : ingredientTypes[0]
    } as Ingredient)
  }

  return (
    <form>
      <VStack spacing='15px'>
        <FormControl id="name">
          <FormLabel>name</FormLabel>
          <Input
            maxLength={255}
            value={props.ingredient.name} 
            onChange={ e => props.onChange({
                ...props.ingredient,
                name : e.currentTarget.value
              } as Ingredient)
            }
          />
        </FormControl>
        <FormControl id="value">
          <FormLabel>value</FormLabel>
          <NumberInput
            value={typeof (props.ingredient as any).__value === 'string' ? (props.ingredient as any).__quantity : props.ingredient.value || ''}
            step={0.01}
            min={0}
            onChange={ e => props.onChange({
                ...props.ingredient,
                value : Number(e),
                __value : e,
              } as Ingredient)
            }
          >
            <NumberInputField />
            <NumberInputStepper>
              <NumberIncrementStepper />
              <NumberDecrementStepper />
            </NumberInputStepper>
          </NumberInput>
        </FormControl>
        <FormControl id="fractional quantity">
          <FormLabel>allow fractional quantity</FormLabel>
          <Switch isChecked={props.ingredient.allowFractional === undefined ? true : !!props.ingredient.allowFractional} onChange={e => {
            let answer = props.ingredient.allowFractional === undefined ? true : !!props.ingredient.allowFractional;
            props.onChange({
              ...props.ingredient,
              allowFractional : !answer
            } as Ingredient)
          }}/>
          <FormHelperText>if set to true, whatever the quantity the user puts in will be floored to the nearest int</FormHelperText>
        </FormControl>
        <FormControl id="types">
          <FormLabel>ingredient type</FormLabel>
          <Select value={props.ingredient.ingredientType} onChange={ e =>{
            props.onChange({
              ...props.ingredient,
              ingredientType : e.currentTarget.value
            } as Ingredient)
          }}>
            {
              ingredientTypes.map(item => (
                <option value={item} key={item}>
                  {item}
                </option>
              ))
            }
          </Select>
          <FormHelperText>classifies an ingredient for promotional reasons</FormHelperText>
        </FormControl>
      </VStack>
    </form>
  )
}

export default IngredientsCreationModal;