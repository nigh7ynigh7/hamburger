import { Box, Button, Center, Heading, Input, Tooltip, VStack, Text, useDisclosure, Table, Tbody, Tr, Td, Thead } from "@chakra-ui/react";
import React, { useState } from "react";
import { Route, Switch } from "react-router-dom";
import OrderSummary from "../../ordering/OrderSummary";

interface Props {
  order ?: Order;
}

export const OrderDetailsModal : React.FC<Props> = (props) => {
  if(!props.order) {
    return <Text>nothing selected</Text>
  }
  return (
    <OrderSummary order={props.order} />
  )
}

export default OrderDetailsModal;