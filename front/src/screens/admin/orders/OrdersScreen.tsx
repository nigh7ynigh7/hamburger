import { DeleteIcon, EditIcon, ViewIcon } from "@chakra-ui/icons";
import { Box, Button, Center, Heading, Input, Tooltip, VStack, Text, HStack, Table, Thead, Tr, Th, useToast, Tbody, Spinner, Td, IconButton, useDisclosure, Breadcrumb, BreadcrumbItem, BreadcrumbLink } from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import { Link, Route, Switch } from "react-router-dom";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
} from "@chakra-ui/react"
import {
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverHeader,
  PopoverBody,
  PopoverFooter,
  PopoverArrow,
  PopoverCloseButton,
} from "@chakra-ui/react"
import OrderDetailsModal from "./OrderDetailsModal";
import { HAMBURGER_API, ORDER_API } from "../../../api/CrudCollection";
import * as coreAdminRoutes from '../../../constants/RouterAdminConstants';



export const OrdersScreen : React.FC<any> = (props) => {
  const toast = useToast();
  const [ page, setPage ] = useState(0);
  const [ times, setTime ] = useState(0);
  const [ saving, setSaving ] = useState(false);
  const [ pagedResult, setPagedResult ] = useState<PagedContent<Order>>();
  const [ activeHam, setActiveHam ] = useState<Partial<Order>>();
  const detailsModal = useDisclosure();

  useEffect(() => {
    ORDER_API.getPaged(page)
      .then(item => setPagedResult(item))
      .catch(err => {
        toast({
          title : 'failed to load page',
          position : 'top-right',
          isClosable : true,
        });
      })
  }, [page, times]);

  
  if(!pagedResult) {
    return (
      <Center>
        <Spinner
          thickness="4px"
          speed="0.65s"
          emptyColor="gray.200"
          color="blue.500"
          size="xl"
        />
      </Center>
    )
  }

  const content = pagedResult.content;

  return (
    <>
      <VStack>
        <Heading as='h3' w='100%'>Orders</Heading>
        <Breadcrumb w='100%' p='10px' borderWidth='1px' borderRadius='3px'>
          <BreadcrumbItem>
            <BreadcrumbLink>
              <Link to={coreAdminRoutes.ADMIN_ROOT}>
                Admin
              </Link>
            </BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumb>
        <Table>
          <Thead>
            <Tr>
              <Th>Who ordered</Th>
              <Th>Hamburger</Th>
              <Th>Actions</Th>
            </Tr>
          </Thead>

          <Tbody>
            {
              content.map((item, i) => {
                return (
                  <Tr>
                    <Td>{item.name}</Td>
                    <Td>{item.selectedHamburger?.name}</Td>
                    <Td>
                      <IconButton 
                          size='sm'
                          icon={<ViewIcon />}
                          aria-label='view'
                          onClick={(e) => {
                            setActiveHam(item);
                            detailsModal.onOpen();
                          }}
                      />
                    </Td>
                  </Tr>
                )
              })
            }          
          </Tbody>
        </Table>
      </VStack>

      <Modal isOpen={detailsModal.isOpen} onClose={detailsModal.onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Details: order</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <OrderDetailsModal order={activeHam as Order} />
          </ModalBody>
          <ModalFooter>
            <Button variant="ghost" onClick={detailsModal.onClose}>ok</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  )
}

export default OrdersScreen;