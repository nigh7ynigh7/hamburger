
import { Box, Button, Center, Heading, Input, Tooltip, VStack, Text, useDisclosure, Table, Tbody, Tr, Td, FormControl, FormLabel, FormHelperText, Select, Thead, Th } from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import { Route } from "react-router-dom";
import {
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
} from "@chakra-ui/react"
import { Switch } from "@chakra-ui/react"
import { IngredientTypeApiInstance } from "../../../api/EnumCollections";
import { INGREDIENT_API } from "../../../api/CrudCollection";
import { CUIAutoComplete } from "chakra-ui-autocomplete";


const toVal = (h : Identifiable<number> & Named) => ({label : h.name, value : String(h.id)});

interface Props {
  hamburger : Partial<Hamburger>;
  onChange : (ingredient : Hamburger) => void;
}

export const HamburgerCreationScreen : React.FC<Props> = (props) => {

  const hamburger = props.hamburger || {}
  const [ingredients, setIngredients] = useState<HamburgerIngredient[]>([]);


  const [ingredientsApi, setIngredientsApi] = useState<Ingredient[]>([]);
  const [ingredientQuery, setIngredientQuery] = useState<string>('');

  useEffect(() => {
    INGREDIENT_API.getPaged(0, ingredientQuery).then(items => setIngredientsApi(items.content || []));
  }, [ingredientQuery]);

  useEffect(() => {
    setIngredients(() => [...(hamburger.hamburgerIngredients || [])])
  }, [hamburger])
  
  console.log(hamburger, ingredients)

  return (
    <form>
      <VStack spacing='15px'>
        <FormControl id="name">
          <FormLabel>name</FormLabel>
          <Input
            maxLength={255}
            value={hamburger.name} 
            onChange={ e => props.onChange({
                ...hamburger,
                name : e.currentTarget.value
              } as Hamburger)
            }
          />
        </FormControl>
        <Box width='100%'>
          {/* TODO fix multiple selection error */}
          <CUIAutoComplete
            label={''}
            placeholder={'ingredient'}
            hideToggleButton={true}
            disableCreateItem={true}
            labelStyleProps={{display : 'none'}}
            onSelectedItemsChange={item => {
              if(item.selectedItems && item.selectedItems.length) {
                
                let resp  = (item.selectedItems || []).map(lbvl => {
                  let key = Number(lbvl.value);
                  let found = (hamburger.hamburgerIngredients || [])
                    .find(i => i.ingredient?.id == key);
                  if(found) return found;
                  let apifound = ingredientsApi.find(item => item.id == key)
                  return {
                    quantity : 0,
                    ingredient : apifound
                  }
                }) as HamburgerIngredient[];

                console.log('changed')
                
                props.onChange({
                  ...hamburger,
                  hamburgerIngredients : [...(resp || [])],
                } as Hamburger);
              }
              else {
                props.onChange({
                  ...hamburger,
                  hamburgerIngredients : [],
                } as Hamburger);
              }
            }}
            optionFilterFunc={(_, query) => {
              setIngredientQuery(query || '');
              return ingredientsApi.map(toVal);
            }}
            items={ingredientsApi.map(toVal)}
            selectedItems={
              (hamburger.hamburgerIngredients || [])
                .map(hi => hi.ingredient)
                .map(toVal)
            }
          />
        </Box>
        <Table size='sm'>
          <Thead>
            <Tr>
              <Th>Ingredient</Th>
              <Th>Quantity</Th>
            </Tr>
          </Thead>
          <Tbody>
            {
              ingredients.map((item, i) => (
                <Tr key={`index_${i}_id_${item.id}_ingredient_${item.ingredient?.id}`}>
                  <Td>{item.ingredient?.name}</Td>
                  <Td>
                    <NumberInput 
                      value={typeof (item as any).__quantity === 'string' ? (item as any).__quantity : item.quantity || ''}
                      step={0.01}
                      onChange={ch => {
                        item.quantity = Number(ch);
                        (item as any).__quantity = ch;
                        ingredients[i] = item;
                        props.onChange({
                          ...hamburger,
                          hamburgerIngredients : [... ingredients],
                        } as Hamburger);
                      }}
                    >
                      <NumberInputField />
                      <NumberInputStepper>
                        <NumberIncrementStepper />
                        <NumberDecrementStepper />
                      </NumberInputStepper>
                    </NumberInput>
                  </Td>
                </Tr>
              ))
            }
          </Tbody>
        </Table>
      </VStack>
    </form>
  )
}

export default HamburgerCreationScreen;