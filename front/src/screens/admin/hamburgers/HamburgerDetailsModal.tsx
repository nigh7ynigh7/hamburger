import { Box, Button, Center, Heading, Input, Tooltip, VStack, Text, useDisclosure, Table, Tbody, Tr, Td, Thead } from "@chakra-ui/react";
import React, { useState } from "react";
import { Route, Switch } from "react-router-dom";

interface Props {
  hamburger ?: Hamburger;
}

export const HamburgerDetailsModal : React.FC<Props> = (props) => {
  if(!props.hamburger) {
    return <Text>nothing selected</Text>
  }
  return (
    <VStack>
      <Table variant='unstyled' size='sm' width='100%'>
        <Tbody>
          <Tr>
            <Td>Id</Td>
            <Td>{props.hamburger.id}</Td>
          </Tr>
          <Tr>
            <Td>Name</Td>
            <Td>{props.hamburger.name}</Td>
          </Tr>
        </Tbody>
      </Table>
      <Table >
        <Tbody>
          <Thead>
            <Tr>
              <Td>Ingredient</Td>
              <Td>Value</Td>
              <Td>Quantity</Td>
            </Tr>
          </Thead>
          <Tbody>
            {
              (props.hamburger.hamburgerIngredients || []).map(item => (
                <Tr key={item.id}>
                  <Td>{item.ingredient?.name}</Td>
                  <Td>{item.ingredient?.value}</Td>
                  <Td>{item.quantity || 0}</Td>
                </Tr>
              ))
            }
          </Tbody>
        </Tbody>
      </Table>
    </VStack>
  )
}

export default HamburgerDetailsModal;