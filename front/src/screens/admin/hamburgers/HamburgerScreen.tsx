import { DeleteIcon, EditIcon, ViewIcon } from "@chakra-ui/icons";
import { Box, Button, Center, Heading, Input, Tooltip, VStack, Text, HStack, Table, Thead, Tr, Th, useToast, Tbody, Spinner, Td, IconButton, useDisclosure, Breadcrumb, BreadcrumbItem, BreadcrumbLink } from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import { Link, Route, Switch } from "react-router-dom";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
} from "@chakra-ui/react"
import {
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverHeader,
  PopoverBody,
  PopoverFooter,
  PopoverArrow,
  PopoverCloseButton,
} from "@chakra-ui/react"
import HamburgerDetailsModal from "./HamburgerDetailsModal";
import HamburgerCreationModal from "./HamburgerCreationModal";
import { HAMBURGER_API } from "../../../api/CrudCollection";
import * as coreAdminRoutes from '../../../constants/RouterAdminConstants';



export const HamburgerScreen : React.FC<any> = (props) => {
  const toast = useToast();
  const [ page, setPage ] = useState(0);
  const [ query, setQuery ] = useState('');
  const [ times, setTime ] = useState(0);
  const [ saving, setSaving ] = useState(false);
  const [ pagedResult, setPagedResult ] = useState<PagedContent<Hamburger>>();
  const [ activeHam, setActiveHam ] = useState<Partial<Hamburger>>();
  const detailsModal = useDisclosure();
  const modifyModal = useDisclosure();

  useEffect(() => {
    HAMBURGER_API.getPaged(page, query)
      .then(item => setPagedResult(item))
      .catch(err => {
        toast({
          title : 'failed to load page',
          position : 'top-right',
          isClosable : true,
        });
      })
  }, [page, query, times])


  
  if(!pagedResult) {
    return (
      <Center>
        <Spinner
          thickness="4px"
          speed="0.65s"
          emptyColor="gray.200"
          color="blue.500"
          size="xl"
        />
      </Center>
    )
  }

  const addTimes = () => setTime(i => i + 1);

  const content = pagedResult.content;

  return (
    <>
      <VStack>
        <Heading as='h3' w='100%'>Hamburgers</Heading>
        <Breadcrumb w='100%' p='10px' borderWidth='1px' borderRadius='3px'>
          <BreadcrumbItem>
            <BreadcrumbLink>
              <Link to={coreAdminRoutes.ADMIN_ROOT}>
                Admin
              </Link>
            </BreadcrumbLink>
          </BreadcrumbItem>
          <BreadcrumbItem>
            <BreadcrumbLink>
              <Link to={coreAdminRoutes.HAMBURGERS}>
                Hamburger
              </Link>
            </BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumb>
        <HStack w='100%'>
          <Input
            value={query}
            placeholder='search'
            onChange={e => setQuery(e.currentTarget.value)}
          />
          <Button onClick={() => {

            setActiveHam({});
            modifyModal.onOpen();
          }}>new</Button>
        </HStack>
        <Table>
          <Thead>
            <Tr>
              <Th>Hamburger</Th>
              <Th isNumeric>Ingredients</Th>
              <Th>Actions</Th>
            </Tr>
          </Thead>

          <Tbody>
            
            {
              content.map((item, i) => {
                return (
                  <Tr>
                    <Td>{item.name}</Td>
                    <Td isNumeric>{item?.hamburgerIngredients.length || 0}</Td>
                    <Td>
                      <IconButton 
                          size='sm'
                          icon={<EditIcon />}
                          aria-label='view'
                          onClick={(e) => {
                            setActiveHam(item);
                            modifyModal.onOpen();
                          }}
                      />
                      {' '}
                      <IconButton 
                          size='sm'
                          icon={<ViewIcon />}
                          aria-label='view'
                          onClick={(e) => {
                            setActiveHam(item);
                            detailsModal.onOpen();
                          }}
                      />
                      {' '}
                      <Popover>
                        <PopoverTrigger>
                          <IconButton 
                              size='sm'
                              icon={<DeleteIcon />}
                              aria-label='delete'
                          />
                        </PopoverTrigger>
                        <PopoverContent>
                          <PopoverArrow />
                          <PopoverCloseButton />
                          <PopoverHeader>are you sure?</PopoverHeader>
                          <PopoverBody>
                            <p>this action cannot be undone</p>
                            <Button 
                              onClick={() => {
                                HAMBURGER_API.delete(item.id)
                                  .then(() => addTimes())
                                  .catch(err => {
                                    toast({
                                      title : 'failed to delete hamburger',
                                      position : 'top-right',
                                      isClosable : true,
                                    });
                                  });
                              }}
                            >
                              delete
                            </Button>
                          </PopoverBody>
                        </PopoverContent>
                      </Popover>
                    </Td>
                  </Tr>
                )
              })
            }          
          </Tbody>
        </Table>
      </VStack>

      <Modal isOpen={detailsModal.isOpen} onClose={detailsModal.onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Details: hamburger</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <HamburgerDetailsModal hamburger={activeHam as Hamburger} />
          </ModalBody>
          <ModalFooter>
            <Button variant="ghost" onClick={detailsModal.onClose}>ok</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
      
      <Modal isOpen={modifyModal.isOpen} onClose={modifyModal.onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Modal Title</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            {
              activeHam ? (
                <HamburgerCreationModal hamburger={activeHam} onChange={(i) => {
                  setActiveHam(i);
                }}/>
              ) : undefined
            }
          </ModalBody>
          <ModalFooter>
            <Button colorScheme="blue" mr={3}
              isDisabled={
                !activeHam
                || !activeHam.name
                || !activeHam.hamburgerIngredients
                || !activeHam.hamburgerIngredients.length
                || activeHam.hamburgerIngredients.some(i => !i.ingredient.id || !i.quantity)
              }
              isLoading={saving}
              onClick={() => {
                setSaving(true);
                if(activeHam){
                  HAMBURGER_API.save(activeHam)
                    .then(() => {
                      setActiveHam(undefined);
                      modifyModal.onClose();
                      addTimes();
                    })
                    .catch(err => {
                      toast({
                        title : 'could not save hamburger',
                        position : 'top-right',
                        isClosable : true,
                      });
                    })
                    .finally(() => setSaving(false));
                  }
              }}
            >
              save
            </Button>
            <Button variant="ghost" onClick={() => {
              setActiveHam(undefined);
              modifyModal.onClose();
            }}>cancel</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  )
}

export default HamburgerScreen;