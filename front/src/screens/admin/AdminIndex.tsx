import { Box, Button, Center, Heading, Input, Tooltip, VStack } from "@chakra-ui/react";
import React, { useState } from "react";
import { Route, Switch } from "react-router-dom";
import VerticalSpacing from "../../components/VerticalSpacing";
import AdminLoggedIndex from "./AdminLoggedIndex";

export const AdminIndex : React.FC<any> = (props) => {

  const [ username, setUsername ] = useState<String>('');
  const [ typedUsername, setTypedUsername ] = useState<string>('');


  if(username) {
    return (
      <AdminLoggedIndex />
    )
  }

  return (
    <Center>
      <VStack>
        <VerticalSpacing size='20vh'/>
        <Box maxW='500px' w='90vw' boxShadow='base' borderRadius='3'>
          <VStack p='15px'>
            <Heading as='h3'>Admin Login</Heading>
            <Tooltip label='any username will do'>
              <Input placeholder='username' value={typedUsername} onChange={e => setTypedUsername(e.target.value)}></Input>
            </Tooltip>
            <Tooltip label='password field is ignored'>
              <Input placeholder='password' type='password'></Input>
            </Tooltip>
            <Button onClick={() => {
              if(typedUsername) {
                setUsername(typedUsername);
              }
            }}>login</Button>
          </VStack>
        </Box>
      </VStack>
    </Center>
  )
}

export default AdminIndex;