import { Box, Button, Center, Heading, Input, Tooltip, VStack, Text } from "@chakra-ui/react";
import React, { useState } from "react";
import { Route, Switch, Link } from "react-router-dom";
import VerticalSpacing from "../../components/VerticalSpacing";
import AdminLayout from "../../layouts/AdminLayout";
import * as coreAdminRoutes from '../../constants/RouterAdminConstants';
import IngredientsScreen from "./ingredients/IngredientsScreen";
import HamburgerScreen from "./hamburgers/HamburgerScreen";
import OrdersScreen from "./orders/OrdersScreen";


export const AdminLoggedIndex : React.FC<any> = (props) => {
  return (
    <AdminLayout sideMenu={
      <VStack width='100%'>
        <Text color='gray.500' fontWeight='bold'>Admin</Text>
        <Link to={coreAdminRoutes.ADMIN_ROOT} style={{width : '100%'}}>
          <Button w='100%'>home</Button>
        </Link>
        <Link to={coreAdminRoutes.INGREDIENTS} style={{width : '100%'}}>
          <Button w='100%'>ingredients</Button>
        </Link>
        <Link to={coreAdminRoutes.HAMBURGERS} style={{width : '100%'}}>
          <Button w='100%'>hamburgers</Button>
        </Link>
        
        <Link to={'/'} style={{width : '100%'}}>
          <Button w='100%'>leave</Button>
        </Link>
      </VStack>
    }>
      <Switch>
        <Route path={coreAdminRoutes.ADMIN_ROOT} exact={true}
          component={() => {
          return <OrdersScreen /> 
          }}
        />
        <Route path={coreAdminRoutes.INGREDIENTS} exact={true}
          component={() => {
          return <IngredientsScreen />
          }}
        />
        <Route path={coreAdminRoutes.HAMBURGERS} exact={true}
          component={() => {
          return <HamburgerScreen />
          }}
        />
      </Switch>
    </AdminLayout>
  )
}

export default AdminLoggedIndex;