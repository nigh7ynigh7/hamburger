import { ChakraProvider } from '@chakra-ui/react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import * as coreRoutes from "./constants/RouterConstants";
import * as adminRoutes from "./constants/RouterAdminConstants";
import OrderIndex from './screens/OrderIndex';
import AdminIndex from './screens/admin/AdminIndex';
import React from 'react';

function App() {
  return (
    <ChakraProvider>
      <Router>
        <Switch>
          <Route strict={false} path={adminRoutes.ADMIN_ROOT} >
            <React.Suspense fallback={<div/>}>
              <AdminIndex />
            </React.Suspense>
          </Route>
          <Route strict={false} path={coreRoutes.ROOT}>
            <OrderIndex />
          </Route>
        </Switch>
      </Router>
    </ChakraProvider>
  );
}

export default App;
