
const ROOT = "/admin";

export const ADMIN_ROOT = ROOT;

export const HAMBURGERS = ROOT + '/hamburgers';
export const INGREDIENTS = ROOT + '/ingredients';
export const ORDERS = ROOT + '/orders';



