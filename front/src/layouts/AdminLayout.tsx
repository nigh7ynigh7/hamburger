import React from "react";
import { Box, Center, Container, Flex, Image } from "@chakra-ui/react";

import logo from '../shared/images/hamburger.png';
import VerticalSpacing from "../components/VerticalSpacing";

interface AdminLayoutProps {
  sideMenu ?: React.ReactNode;
}

export const AdminLayout : React.FC<AdminLayoutProps> = (props) => {

  return (
    <Flex align='flex-start'>
      <Center w='250px' borderLeft='1px' p='10px'>
        {props.sideMenu}
      </Center>
      <Box flex='1' p='10px'>
        {props.children}
      </Box>
    </Flex>
  )
}

export default AdminLayout;