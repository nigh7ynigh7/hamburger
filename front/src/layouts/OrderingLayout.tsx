import React from "react";
import { Box, Center, Container, Image } from "@chakra-ui/react";

import logo from '../shared/images/hamburger.png';
import VerticalSpacing from "../components/VerticalSpacing";

export const OrderingLayout : React.FC<any> = (props) => {
  return (
    <Container maxW='container.md'>
      <VerticalSpacing size={'10vh'} />
      <Center>
        <Image src={logo} ></Image>
      </Center>
      <VerticalSpacing size={30} />
      <Box borderRadius='3px' borderWidth='1px' padding='10px'>
        {props.children}
      </Box>
      <VerticalSpacing size={'20vh'}/>
    </Container>
  )
}

export default OrderingLayout;