import { CrudApi } from "./abstraction/Crud";

export class HamburgerCrud extends CrudApi<number, Hamburger> {
  validate(item: Partial<Hamburger>): void {
    if(!item.name) throw new Error('NO_NAME');
    if(!item.hamburgerIngredients || !item.hamburgerIngredients.length) throw new Error('NO_INGREDIENTS');
  }
}

export const HAMBURGER_API = new HamburgerCrud('/hamburger');

export class IngredientCrud extends CrudApi<number, Ingredient> {
  validate(item: Partial<Ingredient>): void {
    if(!item.name) throw new Error('NO_NAME');
    if(!item.value) throw new Error('NO_VALUE');
    if(!item.allowFractional) item.allowFractional = false;
  }
}

export const INGREDIENT_API = new IngredientCrud('/ingredients');

export class HamburgerIngredientCrud extends CrudApi<number, HamburgerIngredient> {
  validate(item: Partial<HamburgerIngredient>): void {
    if(!item.hamburger) throw new Error('NO_HAMBURGER');
    if(!item.ingredient) throw new Error('NO_INGREDIENT');
    if(typeof item.quantity != 'number' || item.quantity < 0)  throw new Error('NO_QUANTITY');
  }
}

export const HAMBURGER_INGREDIENT_API = new HamburgerIngredientCrud('/hamburger');

export class OrderCrud extends CrudApi<number, Order> {
  validate(item: Partial<Order>): void {
    throw new Error('NOT_ALLOWED');
  }

  async delete(id : number) : Promise<Order> {
    throw new Error('NOT_ALLOWED');
  }
}

export const ORDER_API = new OrderCrud('/orders');


export class IngredientPerOrderCrud extends CrudApi<number, IngredientPerOrder> {
  validate(item: Partial<IngredientPerOrder>): void {
    throw new Error('NOT_ALLOWED');
  }
  
  async delete(id : number) : Promise<IngredientPerOrder> {
    throw new Error('NOT_ALLOWED');
  }
}

export const ORDER_INGREDIENT_API = new IngredientPerOrderCrud('/order-ingredients');