import { EnumApi } from "./abstraction/Enums";

export const IngredientTypeApiInstance = new EnumApi<IngredientType>('/ingredient-types');