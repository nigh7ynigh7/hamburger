import { jsonOrFail } from "../utils/ItemResponse";

const SERVER =  process.env.REACT_APP_API || 'http://localhost:8080';

export const sendOrder = async (order : CompactUserRequest) : Promise<string> => {
  if(!order
     || !order.hamburgerId
     || !order.forWhom
     || order.overwriteIngredients 
     && order.overwriteIngredients.some(i => !i.ingredientId || i.quantity < 0 )) {
    throw new Error('INVALID_ORDER');
  }

  return (
    await fetch(SERVER + '/place-order', {
      body : JSON.stringify(order),
      method : 'post',
      headers: {
        'Content-Type': 'application/json'
      },
    })
    .then(item => {
      if(!item.ok) {
        throw new Error(item.statusText);
      }
      return item.text();
    })
  ) as string
};

export const getPlacedOrder = async (input : string) : Promise<Order> => {
  if(!input) {
    throw new Error('NO_INPUT');
  }

  return (
    await fetch(SERVER + '/your-order/' + input, {
      method : 'get',
    }).then(jsonOrFail)
  ) as Order
};