import { jsonOrFail } from "../../utils/ItemResponse";


const SERVER =  process.env.REACT_APP_API || 'http://localhost:8080';

export class EnumApi<T extends string> {
  private path = '/types';
  constructor(path : string) {
    if(!path) throw new Error('bad path');
    this.path = this.path + "/" + path;
    this.path = this.path.replace(/\/+/g, '/');
  }

  private getPath() {
    return SERVER + this.path;
  }

  async getEnum() : Promise<T[]> {
    return (await fetch(this.getPath(), { method : 'get' }).then(jsonOrFail)) as T[]
  }
}