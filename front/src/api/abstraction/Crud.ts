import { jsonOrFail } from "../../utils/ItemResponse";

const SERVER =  process.env.REACT_APP_API || 'http://localhost:8080';

export abstract class CrudApi<Y, T extends Identifiable<Y>> {
  private path = '/crud';

  constructor(path : string) {
    if(!path) throw new Error('bad path');
    this.path = this.path + "/" + path;
    this.path = this.path.replace(/\/+/g, '/');
  }

  // validate() : string[];

  private getPath() {
    return SERVER + this.path;
  }

  async getPaged(page ?: number, query ?: string, size ?: number) : Promise<PagedContent<T>> {
    page = page || 0;
    query = query || "";
    size = size || 50;
    return (await fetch(
      this.getPath() + "?" + new URLSearchParams({ page, query, size } as any).toString(), {
      method : 'get',
      headers: {
        'Content-Type': 'application/json'
      },
    }).then(jsonOrFail)) as PagedContent<T>;
  }

  async getById(id : Y) : Promise<T> {
    if(!id) {
      Promise.reject(new Error('NO_ID'));
    }
    return (await fetch(this.getPath() + '/' + id, { method : 'get' }).then(jsonOrFail)) as T
  }

  async save(obj : Partial<T>) : Promise<T> {
    if(!obj) {
      Promise.reject(new Error('NO_OBJ'));
    }

    const id = obj.id;

    this.validate(obj);

    if(!id) {
      obj.id = undefined;
      return (await fetch(
        this.getPath(),
        {
          method : 'post',
          body : JSON.stringify(obj),
          headers: {
            'Content-Type': 'application/json'
          },
        }).then(jsonOrFail)) as T
    }
    
    return (await fetch(
      this.getPath() + '/' + id,
      {
        method : 'put',
        body : JSON.stringify(obj),
        headers : {
          'Content-Type' : 'application/json'
        }
      }).then(jsonOrFail)) as T
  }

  async delete(id : Y) : Promise<T> {
    if(!id) {
      Promise.reject(new Error('NO_ID'));
    }
    return (await fetch(this.getPath() + '/' + id, { method : 'delete' }).then(jsonOrFail)) as T
  }

  abstract validate(item : Partial<T>) : void;
}