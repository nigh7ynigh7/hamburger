import React from "react";

export const VerticalSpacing : React.FC<{size : number | string}> = (props) => {
  return (
    <div style={{width : '100%', height : props.size}} />
  )
}

export default VerticalSpacing;