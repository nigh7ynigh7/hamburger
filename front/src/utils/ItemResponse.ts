
export const jsonOrFail = async (item : Response) : Promise<any> => {
  if(!item.ok) {
    throw new Error(item.statusText)
  }
  return item.json()
}