# Dextra full stack challenges

* [Hamburgueria](HAMBURGUER.md)

# README (en)

To run, you can either download the packages from the pipeline on the left here
...and just hook them up to your computer manually and run them there using java 11.

Or you do one of the following:

    $ docker-compose up

or...

in the `back` folder

    $ mvn clean package spring-boot:run

and in the `front` folder

    $ REACT_APP_API='http://localhost:8080'  npm run start

## Detail about the front end

Access the `/admin` path to modify existing values.

# README (ptbr)

Para executar o projeto, ou você baixa os arquivos no pipeline ao lado aqui
... e execute os arquivos considerando que você está com java 11 instalado.

Ou você faz o seguinte:

    $ docker-compose up

ou....

no diretório `back`

    $ mvn clean package spring-boot:run

e no dirtório `front`

    $ REACT_APP_API='http://localhost:8080'  npm run start
## Detalhe sobre o front

Acesse o caminho `/admin` para modificar valores existentes.
